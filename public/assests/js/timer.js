function startTimer() {
    var timer = document.getElementById("timer");
    var time = timer.innerHTML;
    var arr = time.split(":");
    var m = arr[0];
    var s = arr[1];
    if (s == 0) {
        if (m == 0) {
            alert("Время вышло");
            //window.location.reload();
            let href = timer.getAttribute('attrHref');
            window.location = href;
            return;
        }
        m--;
        if (m < 10) m = "0" + m;
        s = 59;
    }
    else s--;
    if (s < 10) s = "0" + s;
    document.getElementById("timer").innerHTML = m+":"+s;
    setTimeout(startTimer, 1000);
}
startTimer();
