$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

   $('.dropdownMenuButton').hover(function () {
        $(this).children('.dropdown-menu').show();
   }, function () {
       $(this).children('.dropdown-menu').hide();
   });



    $('#create_string').submit(function (event) {
        if( $('.b-info-before').is(':hidden') ){
            event.preventDefault();
            $('.b-info-before').removeClass('b-info-before__none');
        }
    });

    $('.rating-area').on('click', 'label', function (event) {
         let score = $(this).attr('value');
         let link = $('.rating-area').attr('link');
         let ratingableType = $('.rating-area').attr('ratingable-type');
         let ratingableId = $('.rating-area').attr('ratingable-id');

         set_score(score, link, ratingableType, ratingableId);
    });

    function set_score(score, link, ratingableType, ratingableId)
    {
        let json_data = {"score":score, "ratingableType":ratingableType, "ratingableId": ratingableId};
        $.post(
            link,
            json_data,
            get_answer,
            "json"
        );
    }

    function get_answer(answer)
    {
    }

    $('.form-request-post').submit(function (event){
        event.preventDefault();
        const args = {};
        document.querySelectorAll('[data-component]').forEach((component) => {
            let fieldName = component.getAttribute('name');
            if(component.getAttribute('type') == 'radio'){
                if (component.checked == false){
                    return;
                }
            }
            if (fieldName.indexOf('[]') !== -1) {
                fieldName = fieldName.replace('[]', '');
            }
            if(component.matches('[data-multiples]')){
                let arr = [];
                for(let i = 0; i < component.options.length; i++ ){
                    arr.push(component.options[i].value);
                }
                args[fieldName] = arr;
            } else {
                args[fieldName] = component.value;
            }
        });
        args.action = this.action;
        create_element(args);
    });

    function create_element(data)
    {
        $.post(
            data.action,
            data,
            get_answer_badword,
            "json"
        );
    }

    function get_answer_badword(response)
    {
        if(response.success === true && response.url_redirect){
            document.location.replace(response.url_redirect);
        }
        if(response.success === false){
            alert(response.message);
            //$('.window-error').show();
        }
    }

    $('#checkout-group-element').change(function (input){
        if(this.checked){
            this.value = 1;
        } else {
            this.value = 0;
        }
    });

    $('.js-btnBan').click(function () {
        $('.modal').show();
        $attrId = $(this).attr('attrId');
        $attrNic = $(this).attr('attrNic');
        $('.insert-user_id').val($attrId);
        $('.user_nic_insert').text($attrNic);
    });

    $('.js-btnClose').click(function () {
        $('.modal').hide();
        $('.insert-user_id').val('');
        $('.user_nic_insert').text('');
    });

    $('.jsBan').click(function (){
        let $ban_time = $('#ban_time').val();
        if(!$ban_time) return;

        let $user_id = $('#insert-user_id').val();
        let link = $('#link_user_ban').val();
        let message = $('#js-message').val();

        let json_data = {"userId":$user_id, "banTime":$ban_time, "message":message};

        $.post(
            link,
            json_data,
            get_answer_ban,
            "json"
        );
    });


    function get_answer_ban(answer)
    {
        $('.modal').hide();
        $('.insert-user_id').val('');
        $('.user_nic_insert').text('');
        let select_id = '#ban_until_' + answer.userId;
        $(select_id).text(answer.banDate);
    }


    $('.show_vote_to_remove').click(function (event){
        event.preventDefault();
        $('.block-vote_remove').toggle();
    });



    // var strip_tags = function(str) {
    //     return (str + '').replace(/<\/?[^>]+(>|$)/g, '')
    // };
    // var truncate_string = function(str, chars) {
    //     if ($.trim(str).length <= chars) {
    //         return str;
    //     } else {
    //         return $.trim(str.substr(0, chars)) + '…';
    //     }
    // };
    // var $activate_selectator = $('#activate_selectator4_ajax');
    // $activate_selectator.click(function () {
    //     var $select = $('#select3_ajax');
    //     if ($select.data('selectator') === undefined) {
    //         $select.selectator({
    //             showAllOptionsOnFocus: true,
    //             load: function (search, callback) {
    //                 if (search.length < this.minSearchLength) return callback();
    //                 $.ajax({
    //                     url: '/authors/find/s=' + encodeURIComponent(search) + '',
    //                     type: 'GET',
    //                     dataType: 'json',
    //                     success: function(data) {
    //                         //callback(data.words.slice(0, 100));
    //                     },
    //                     error: function() {
    //                         callback();
    //                     }
    //                 });
    //             },
    //             placeholder: 'Search...',
    //             delay: 300,
    //             minSearchLength: 1,
    //             valueField: 'Id',
    //             textField: 'DisplayWord',
    //             render: {
    //                 option: function (_item, escape) {
    //                     var html = '';
    //                     html += '<div class="selectator_option_title">' + ((typeof _item.DisplayWord !== 'undefined') ? _item.DisplayWord : '') + '</div>';
    //                     html += '<div class="selectator_option_subtitle">' + ((typeof _item.Explanation !== 'undefined') ? truncate_string(escape(strip_tags(_item.Explanation)), 75) : '') + '</div>';
    //                     return html;
    //                 }
    //             }
    //         });
    //         $activate_selectator.val('destroy');
    //     } else {
    //         $select.selectator('destroy');
    //         $activate_selectator.val('activate');
    //     }
    // });
    // $activate_selectator.trigger('click');









    $('.js-data-example-ajax').select2({
        ajax: {
            url: '/authors/find/search',
            dataType: 'json',
        },
        width:"100%",
        language: "ru"

    });







});



