<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' =>  ['auth']],
    function (){
        Route::get('/', 'DashboardController@dashboard')->name('admin.index');
    }
);

Auth::routes();




Route::get('/', 'HomeController@index')->name('home');


Route::get('/elements', 'Elements\ElementsController@elements')->name('elements');
Route::get('/elements/genre/{genre}', 'Elements\ElementsController@elements')->name('elements.genre');


Route::get('/elements/finished/genre/{genre?}/{sort?}', 'Elements\ElementsController@elements_finished_by_genre')->name('elements.finished.genre');

Route::get('/elements/finished/{sort?}/', 'Elements\ElementsController@elements_finished')->name('elements.finished');

Route::get('/elements/group/', 'Elements\ElementsController@elements_group')->name('elements.group')->middleware('auth');
Route::get('/element/create', 'Elements\ElementsController@create')->name('element.create')->middleware('auth');
Route::post('/element/store', 'Elements\ElementsController@store')->name('element.store')->middleware('auth');
Route::get('/element/writing/', 'Elements\ElementsController@writing')->name('writing')->middleware('auth');
Route::get('/element/reader/', 'Elements\ElementsController@reader')->name('reader')->middleware('auth');
Route::get('/element/wrote/', 'Elements\ElementsController@wrote')->name('wrote')->middleware('auth');

Route::get('/element/{element}/', 'Elements\ElementsController@show')->name('element.show');


Route::get('/element/{element}/read', 'Elements\ElementsController@element_finished')->name('element.finished.read');
Route::get('/element/{element}/edit', 'Elements\ElementsController@edit')->name('element.edit')->middleware('auth');

Route::put('/element/{element}/update', 'Elements\ElementsController@update')->name('element.update')->middleware('auth');

Route::put('/element/{element}/destroy', 'Elements\ElementsController@destroy')->name('element.destroy')->middleware('auth');

Route::get('/element/{element}/string_create', 'Elements\ElementStringsController@create')->name('string.create')->middleware('auth', 'isBlocked');

Route::post('/element/string_store', 'Elements\ElementStringsController@store')->name('string.store')->middleware('auth');

Route::get('/elements/draft', 'Elements\ElementsController@elements_draft')->name('elements.draft')->middleware('auth');

Route::post('/user/remove_bookmark', 'BookmarksController@remove_bookmark')->name('remove_bookmark')->middleware('auth');



Route::resource('/admin/genre', 'Admin\AdminGenreController', ['names' =>
    [
        'show'   => 'a_genre.show',
        'store'   => 'a_genre.store',
        'index'   => 'a_genre.index',
        'create'  => 'a_genre.create',
        'destroy' => 'a_genre.destroy',
        'update'  => 'a_genre.update',
        'show'    => 'a_genre.show',
        'edit'    => 'a_genre.edit',
    ]
])->middleware('auth');


Route::resource('/admin/badword', 'Admin\BadWordController',
    ['names' => [
        'index'   => 'a_badword.index',
        'store'   => 'a_badword.store',
        'create'  => 'a_badword.create'
    ]
])->middleware('auth');

Route::post('/admin/badword/{destroy}/destroy', 'Admin\BadWordController@destroy')->name('a_badword.destroy')->middleware('isAdmin','auth');



Route::resource('/admin/user', 'Admin\UserController', ['names' =>
    [
        'show' => 'a_user.show',
        'store'   => 'a_user.store',
        'index'   => 'a_user.index',
        'create'  => 'a_user.create',
        'destroy' => 'a_user.destroy',
        'update'  => 'a_user.update',
        'edit'    => 'a_user.edit'
    ]
])->middleware('isAdmin', 'auth');

Route::post('/admin/user/ban/', 'Admin\UserController@ban')->name('a_user.bun')->middleware('auth');




Route::get('/genres', 'GenresController@genres')->name('genres');
Route::get('/genre/{genre}', 'GenresController@genre')->name('genre')->middleware('auth');

Route::get('/review/{review}/', 'Elements\ReviewsController@show')->name('review.show');


Route::get('/element/{element}/review_create', 'Elements\ReviewsController@create')->name('review.create')->middleware('auth');
Route::post('/element/{element}/review_store', 'Elements\ReviewsController@store')->name('review.store')->middleware('auth');

Route::get('/review/{review}/edit', 'Elements\ReviewsController@edit')->name('review.edit')->middleware('auth', 'can:edit,review');
Route::post('/review/{review}/update', 'Elements\ReviewsController@update')->name('review.update')->middleware('auth', 'can:edit,review');


Route::get('/element/{element}/reviews', 'Elements\ReviewsController@show_reviews_by_element')->name('show_reviews_by_element');
Route::get('/element/{element}/corrections', 'CorrectionsController@show_corrections_by_element')->name('show_corrections_by_element');


Route::get('/element/{element}/correction_create', 'CorrectionsController@create')->name('correction.create')->middleware('auth');
Route::post('/element/{element}/correction_store', 'CorrectionsController@store')->name('correction.store')->middleware('auth');


Route::get('/correction/{correction}/edit', 'CorrectionsController@edit')->name('correction.edit')->middleware('auth', 'can:edit,correction');
Route::post('/correction/{correction}/update', 'CorrectionsController@update')->name('correction.update')->middleware('auth', 'can:edit,correction');


Route::get('/correction/{correction}/', 'CorrectionsController@show')->name('correction.show');


Route::get('/authors/{sort?}', 'User\UserController@index')->name('user.index');
Route::get('/authors/{user}/edit', 'User\UserController@edit')->name('user.edit')->middleware('auth');
Route::get('/authors/{user}/show', 'User\UserController@show')->name('user.show');
Route::post('/authors/{user}/store', 'User\UserController@store')->name('user.store')->middleware('auth');
Route::post('/authors/{user}/update', 'User\UserController@update')->name('user.update')->middleware('auth');

Route::get('/author/{user}/reviews/', 'Elements\ReviewsController@show_reviews_by_user')->name('show_reviews_by_user');
Route::get('/author/{user}/corrections/', 'CorrectionsController@show_corrections_by_user')->name('show_corrections_by_user');

Route::get('/author/reviews_in_work/', 'Elements\ReviewsController@reviews_in_work')->name('reviews_in_work')->middleware('auth');
Route::get('/author/corrections_in_work/', 'CorrectionsController@corrections_in_work')->name('corrections_in_work')->middleware('auth');

Route::get('/authors/find/search', 'User\UserController@search')->name('user.search')->middleware('auth');


Route::get('/admin/article/create', 'Admin\Article\ArticlesController@create')->name('a_article.create')->middleware('auth');
Route::post('/admin/article/store', 'Admin\Article\ArticlesController@store')->name('a_article.store')->middleware('auth');
Route::get('/admin/article/', 'Admin\Article\ArticlesController@index')->name('a_article.index')->middleware('auth');
Route::get('/admin/article/{article}/edit', 'Admin\Article\ArticlesController@edit')->name('a_article.edit')->middleware('auth');
Route::get('/admin/{article}/destroy', 'Admin\Article\ArticlesController@destroy')->name('a_article.destroy')->middleware('auth');
Route::post('/admin/article/{article}/update', 'Admin\Article\ArticlesController@update')->name('a_article.update')->middleware('auth');


Route::get('/article/', 'Article\ArticlesController@index')->name('article.index');
Route::get('/article/{article}/', 'Article\ArticlesController@show')->name('article.show');



Route::post('/ratings/set_rating/', 'RatingController@set_rating')->name('rating.set')->middleware('auth');
//Route::post('/ratings/store/', 'RatingController@store')->name('rating.store')->middleware('auth');
//Route::post('/ratings/update/', 'RatingController@update')->name('rating.update')->middleware('auth');


Route::post('/vote_remove/', 'Elements\ElementVoteRemoveController@vote_remove_set')->name('vote_remove_set')->middleware('auth', 'VoteCheck');

Route::get('/notifications/', 'NotificationsController@list')->name('notifications');

Route::post('/notifications_remove/', 'NotificationsController@remove')->name('notifications_remove')->middleware('auth');




Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'Message\MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'Message\MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'Message\MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'Message\MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'Message\MessagesController@update']);
});

Route::get('/contacts', 'Message\ContactsController@get')->middleware('auth');

Route::get('/chats', 'ChatsController@getOnUser' )->middleware('auth');

Route::post('/createchat/', 'ChatsController@create')->middleware('auth');

Route::get('/chat', function (){
       return view('messages.list');
})->name('chat')->middleware('auth');


Route::get('/ch', function (App\Http\Controllers\ChatsController $chats){
    $chats->getOnUser();
})->middleware('auth');


Route::get('/conversation/{id}', 'ChatsController@getMessagesByChat')->middleware('auth');

Route::get('/cn-sdf/', 'Message\MessagesController@getCountUnreadMessagesByUser')->middleware('auth');

Route::post('/conversation/send', 'ChatsController@send')->middleware('auth');
Route::post('/conversation/quit', 'ChatsController@leave_chat')->middleware('auth');

Route::post('/conversation/read', 'ChatsController@readMessagesByChat')->middleware('auth');
