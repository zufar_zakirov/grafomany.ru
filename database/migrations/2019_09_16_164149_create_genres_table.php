<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genres', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->longText('text')->nullable()->default('');
            $table->string('slug')->unique()->default('')->comment('alias - url');
            $table->integer('letters_for_string')->comment('Кол-во симоволов для строки');
            $table->string('title_meta')->default('');
            $table->string('desc_meta')->default('');
            $table->string('key_meta')->default('');
            $table->tinyInteger('published')->default(1)->comment('опубликован; не опубликован');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genres');
    }
}
