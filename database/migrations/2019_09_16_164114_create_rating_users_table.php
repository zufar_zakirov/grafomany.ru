<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('user_id_rater')->comment('id оценщика');
            $table->biginteger('user_id_rating')->comment('id юзера, которого оценивают');
            $table->tinyInteger('point')->comment('Баллы. От 1 до 3');
            $table->string('type')->comment('Варианты: author, critic, corrector. Тип пользователя');
            $table->biginteger('element_string_id')->comment('id строки, если оценивают автора строки');
            $table->biginteger('review_id')->comment('id критической статьи, если оценивают её');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating_users');
    }
}
