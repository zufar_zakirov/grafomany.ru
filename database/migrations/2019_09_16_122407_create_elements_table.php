<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('parent_id')->default(0)->comment('родитель для корректорского произведения');
            $table->string('name');
            $table->string('message')->nullable()->comment('коменатрий, послание к произведению');
            $table->integer('genre_id');
            $table->string('type' , '80')->nullable()->comment('main, corrected -  произведение или корректировка произведения');
            $table->longText('text_corrected')->nullable()->comment('текст корректорского произведения');
            $table->tinyInteger('blocked')->default(1)->comment('0 - блокировка произведений - для написания произведения');
            $table->biginteger('blocked_user_id')->nullable()->comment('кем заблокировано произведение');
            $table->timestamp('blocked_time')->nullable()->comment('до какого момента заблокировано произведение');
            $table->tinyInteger('finished')->nullable()->default(0)->comment('для законченных произведений - 1');
            $table->string('slug')->comment('alias-url');
            $table->tinyInteger('published')->default(1)->comment('опубликован; не опубликован. 0 - не опубликован');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elements');
    }
}
