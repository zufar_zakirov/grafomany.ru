<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('family')->nullable();

            $table->string('middle_name')->nullable()->comment('Отчество');
            $table->string('birth_day')->nullable()->comment('День рождения');

            $table->string('nic')->nullable()->comment('Ник автора - псевдоним');
            $table->integer('role_id')->nullable()->default(1)->comment('1-юзер, 10-админ');
            $table->tinyInteger('ban')->default(0)->comment('Отметка о бане юзера');
            $table->string('ban_time')->nullable()->comment('До какого момента бан');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->tinyInteger('published')->default(1)->comment('опубликован; не опубликован');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
