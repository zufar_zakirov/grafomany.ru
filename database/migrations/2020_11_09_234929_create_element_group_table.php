<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_group', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->biginteger('element_id')->unsigned()->nullable();
            $table->foreign('element_id')->references('id')->on('elements');

            $table->biginteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->char('offer', 1)->nullable()->comment('принято - Y; отклонено - N; просмотрено - S');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_group');
    }
}
