<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementStringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_strings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('element_id');
            $table->foreign('element_id')->references('id')->on('elements');

            $table->bigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('string');
            $table->tinyInteger('published')->default(1)->comment('опубликован; не опубликован');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_strings');
    }
}
