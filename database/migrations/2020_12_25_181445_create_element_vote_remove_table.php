<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementVoteRemoveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_vote_remove', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->biginteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->biginteger('element_id')->unsigned();
            $table->foreign('element_id')->references('id')->on('elements');

            $table->integer('answer')->comment('0-удаляем,1-оставляе');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_vote_remove');
    }
}
