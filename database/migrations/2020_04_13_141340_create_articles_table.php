<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->longText('text')->nullable()->default('');

            $table->biginteger('cat_article_id')->unsigned();
            $table->foreign('cat_article_id')->references('id')->on('cat_articles');

            $table->biginteger('genre_id')->unsigned();
            $table->foreign('genre_id')->references('id')->on('genres');



            $table->tinyInteger('published')->index();

            $table->string('title_meta')->default('');
            $table->string('desc_meta')->default('');
            $table->string('key_meta')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
