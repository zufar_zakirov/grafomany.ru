<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating_elements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('element_id');
            $table->biginteger('user_id');
            $table->tinyInteger('point')->comment('Баллы. От 1 до 3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating_elements');
    }
}
