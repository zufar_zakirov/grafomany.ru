<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_articles', function (Blueprint $table) {
            $table->bigIncrements('id');


            $table->tinyInteger('published')->index();
            $table->longText('text')->nullable()->default('');
            $table->string('title_meta')->default('');
            $table->string('desc_meta')->default('');
            $table->string('key_meta')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_articles');
    }
}
