@extends('layouts.app', ['title' => $element->e_name])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Групповое неоконченное произведение @endslot
            @slot('parent') главная @endslot
            @slot('active')<a href="{{route('elements.draft')}}">черновики</a> @endslot
            @slot('child') {{ $element->e_name }} @endslot
        @endcomponent


        <div class="row">
            <div class="col-6">
                <h1> {{ $element->e_name }}
                    @if($element->e_group == 1)
                        <sup style="font-size:16px">
                            [<i class="fas fa-users fa-1x"></i>]
                        </sup>
                    @endif

                    @if($element->e_published !== 1)
                        <i class="fas fa-trash-alt"></i>
                    @endif
                </h1>
                <br />

                @isset(Auth::user()->id)
                    @if(!$elementVote)
                        @if($isCoAuthor)
                            <a href="#" class="show_vote_to_remove">голосование на удаление</a>
                            <div class="row block-vote_remove">
                                <div class="col-6">
                                    @include('partials.element_vote_remove')
                                </div>
                            </div>
                        @endif
                    @endif
                @endif
            </div>
            <div class="col-2">
                <b>{{ $element->g_name }}</b> <i>(жанр)</i>
                <br />
                <b>{{ $element->u_nic }}</b> <i>(автор)</i>
            </div>
            <div class="col-2">
                {{ $element->message }}
                <br />
                <i>(послание)</i>
            </div>
            <div class="col-2">
                произведение в черновиках
            </div>
        </div>






        <div class="row">
            @foreach( $element_strings as $element_string )
                <div class="col-8">
                    {{ $element_string->string }}
                </div>
                <div class="col-2">
                    <a href="{{ route('user.show', $element_string->user_id)  }}">
                        {{ $element_string->u_nic }}
                    </a>
                </div>
                <div class="col-2">
                    {{ date('H:i | d.m.Y ', strtotime($element_string->created_at)) }}
                </div>
            @endforeach
        </div>
        @if( count($users_group) )
            <div class="row">
                <div class="col-2">
                    Приглашенные авторы:
                </div>
                <div class="col-10">
                    @foreach($users_group as $user_group)
                        <a href="{{ route('user.show', $user_group->id) }}" class="btn btn-sm btn-info">
                            {{ $user_group->nic }}
                        </a>
                    @endforeach
                </div>
            </div>
        @endif


    </div>
@endsection
