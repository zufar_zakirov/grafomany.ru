@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Не опубликовано @endslot
            @slot('parent') главная @endslot
            @slot('active') <a href="{{route('elements.finished')}}">оконченные произведения</a> @endslot
        @endcomponent



        <div class="row">
            <div class="col-12">
                <h3> У вас нет доступа к этому произведению </h3>
            </div>
        </div>
    </div>
@endsection
