@extends('layouts.app')

@section('content')
    <div class="container">
        @push('scripts')
            <script src="{{ asset('assests/js/timer.js') }}"></script>
        @endpush

        @component('components.breadcrumb')
            @slot('title') Добавляем свою строку @endslot
            @slot('parent') главная @endslot
            @slot('active') пишем текст @endslot
        @endcomponent

            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between">
                            <div>Ваш текст</div>
                            <div class="timer">
                                <span class="btn btn-sm text-danger" id="timer" attrHref="{{ route('element.show', $element->e_id ) }}">05:00</span>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('string.store') }}" method="post" enctype="multipart/form-data" id="create_string" class="create-element  form-request-post">
                                {{csrf_field()}}
                                <div class="form-group row">
                                    <input name="string"
                                           placeholder="Текст"
                                           class="form-control"
                                           id="string"
                                           maxlength="{{ $element->letters_for_string }}"
                                           required
                                           data-component
                                    />
                                </div>
                                <div class="form-group row justify-content-center b-info-before b-info-before__none">
{{--                                    <i class="fas fa-child fa-1x text-danger"></i>--}}
                                    <div class="pr-2">
                                        <i class="fas fa-info-circle fa-1x text-danger"></i>
                                    </div>
                                    <div class="">
                                        <span class="text-success"> Вы проверели ошибки?</span>
                                    </div>
                                </div>
                                <div class="form-group row justify-content-end">
                                    <input type="hidden" name="element_id" value="{{ $element->e_id }}" data-component />
                                    <input type="submit" value="Записать" class="btn btn-success" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        <div class="row">
            <div class="col-6">
                <h1> {{ $element->e_name }} </h1>
            </div>
            <div class="col-2">
                <b>{{ $element->g_name }}</b>
                <br />
                (жанр)
            </div>
            <div class="col-2">
                <b>{{ $element->user_name }}</b>
                <br />
                (автор)
            </div>
            <div class="col-2">

            </div>
        </div>
        <div class="row">
            @foreach( $element_strings as $element_string )
                <div class="col-9">
                    {{ $element_string->string }}
                </div>
                <div class="col-3">
                    <a href="/user/{{ $element_string->user_id }}">
                        {{ $element_string->nic }}
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
