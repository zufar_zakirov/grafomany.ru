@extends('layouts.app', ['title' => 'Неоконченные произведения'])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Неоконченные произведения @endslot
            @slot('parent') главная @endslot
            @slot('active') неоконченные произведения @endslot
        @endcomponent

        <div class="row justify-content-end">
            <div class="col-6">
                <form method="get" action="{{ route('elements') }}">
                    <input type="text" name="q" value="{{$q}}" placeholder="поиск">
                    <input type="submit" value="поиск" class="">
                </form>
            </div>
            <div class="col-6">
                @include('partials.filter_genre', ['route_filter' => 'elements'])
            </div>
        </div>

        <div class="row">
            <div class="col-5">
                Наименование
            </div>
            <div class="col-3">
                Осталось времени
            </div>
            <div class="col-2">
                Жанр
            </div>
            <div class="col-2">
                Дата создания
            </div>
        </div>
        @forelse($elements as $element )
        <div class="row">
            <div class="col-5">
                <a href="{{ route('element.show', $element->e_id)  }}">
                    {{ $element->e_name }}
                </a>
            </div>
            <div class="col-3">
                {{ $element->remains->number }}
                {{ $element->remains->word }}
            </div>
            <div class="col-2">
                {{$element->g_name}}
            </div>
            <div class="col-2">
                {{ date('d.m.Y H:i', strtotime($element->e_created_at)) }}
            </div>
        </div>

        @empty
            <div class="row">
                <div class="col-8">
                    <div class="text-center">
                        Здесь пока ничего нет. (
                    </div>
                </div>
            </div>
        @endforelse

        <div class="footer">
            <nav class="Page navigation example">
                <ul class="pagination">
                    {{$elements->links()}}
                </ul>
            </nav>
        </div>

    </div>
@endsection
