@extends('layouts.app', ['title' => $element->e_name])

@section('content')
    <div class="container">

        @if($element->e_group == 1)
            @component('components.breadcrumb')
                @slot('title') Групповое неоконченное произведение @endslot
                @slot('parent') главная @endslot
                @slot('active')<a href="{{route('elements.group')}}">групповые произведения</a> @endslot
                @slot('child') {{ $element->e_name }} @endslot
            @endcomponent
        @else
            @component('components.breadcrumb')
                @slot('title') Неоконченное произведение @endslot
                @slot('parent') главная @endslot
                @slot('active')<a href="{{route('elements')}}">неоконченные произведения</a> @endslot
                @slot('child') {{ $element->e_name }} @endslot
            @endcomponent
        @endif

        <div class="row">
            <div class="col-6">
                <h1> {{ $element->e_name }}
                    @if($element->e_group == 1)
                        <sup style="font-size:16px">
                        [<i class="fas fa-users fa-1x"></i>]
                        </sup>
                    @endif
                </h1>
                <br />
                <i class="fas fa-hourglass-half"></i>
                {{ $element->remains->number }}
                {{ $element->remains->word }} <i>(осталось до публикации)</i>
                <br />
                @isset(Auth::user()->id)
                    @if(!$elementVote)
                        @if($isCoAuthor)
                            <a href="#" class="show_vote_to_remove">
                                голосование на удаление ({{$element_vote_remove_count}} проголосовавших)
                            </a>
                            <div class="row block-vote_remove">
                                <div class="col-6">
                                    @include('partials.element_vote_remove')
                                </div>
                            </div>
                        @endif
                    @endif
                @endif
            </div>
            <div class="col-2">
                <b>{{ $element->g_name }}</b> <i>(жанр)</i>
                <br />
                <b>{{ $element->u_nic }}</b> <i>(автор)</i>
            </div>
            <div class="col-2">
                {{ $element->message }}
                <br />
                <i>(послание)</i>
            </div>
            <div class="col-2">
                @if ( $element->blocked_time > Carbon\Carbon::now() && $element->blocked_user_id != Auth::user()->id )
                    @php
                        $now = Carbon\Carbon::now();
                        $diff_in_mitutes = $now->diffInMinutes( $element->blocked_time );
                    @endphp
                    <i class="fa fa-clock"></i> <abbr title="добавление возможно через {{$diff_in_mitutes}} минут">В редактуре</abbr>
                @elseif($possibility_write_for_user)
                    <?php
                    $time_head = 'мин.';
                    $time_left = '';
                    if ($possibility_write_for_user>60){
                        $time_left = round($possibility_write_for_user / 60);
                        $time_head = 'час.';
                    }
                    ?>
                    <i class="fa fa-clock"></i> Вы сможете написать через {{$time_left}} {{$time_head}}
                @elseif( isset( Auth::user()->id ) && time() > Auth::user()->ban_time )
                    <a href="{{ route('string.create', $element->e_id) }}"> <i class="fas fa-pen-fancy"></i> Добавить строку</a>
                @elseif( isset(Auth::user()->ban_time) && time() < Auth::user()->ban_time )
                    У вас бан до
                    <br />
                    <b>{{date('H:i m.d.Y', Auth::user()->ban_time)}}</b>
                @else
                    <abbr title="Авторизуйтесь или зарегистрируйтесь"> <i class="fas fa-pen-fancy"></i> Добавить строку</abbr>
                @endif
            </div>
        </div>






        <div class="row">
            @foreach( $element_strings as $element_string )
                <div class="col-8">
                    {{ $element_string->string }}
                </div>
                <div class="col-2">
                    <a href="{{ route('user.show', $element_string->user_id)  }}">
                        {{ $element_string->u_nic }}
                    </a>
                </div>
                <div class="col-2">
                    {{ date('H:i | d.m.Y ', strtotime($element_string->created_at)) }}
                </div>
            @endforeach
        </div>
        @if( count($users_group) )
            <div class="row">
                <div class="col-2">
                    Приглашенные авторы:
                </div>
                <div class="col-10">
                    @foreach($users_group as $user_group)
                        <a href="{{ route('user.show', $user_group->id) }}" class="btn btn-sm btn-info">
                            {{ $user_group->nic }}
                        </a>
                    @endforeach
                </div>
            </div>
        @endif


    </div>
@endsection
