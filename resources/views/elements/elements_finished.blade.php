@extends('layouts.app', ['title' => 'Оконченные произведения' ])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Оконченные произведения@endslot
            @slot('parent') главная @endslot
            @slot('active') оконченные произведения @endslot
        @endcomponent

        <div class="row justify-content-end">
            <div class="col-4">
                <form method="get" action="{{ route('elements.finished') }}">
                    <input type="text" name="q" value="{{$q}}" placeholder="поиск">
                    <input type="submit" value="поиск" class="">
                </form>
            </div>
            <div class="col-4">
                <div class="dropdown dropdownMenuButton">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if( $field_sort_selected )
                            {{$field_sort_selected}}
                        @else
                            Сортировка по
                        @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @if($genre_selected)
                            <a class="dropdown-item" href="{{ route('elements.finished') }}/?g_id={{$genre_selected->id}}&sort=sort-date-desc">Сначала новые</a>
                            <a class="dropdown-item" href="{{ route('elements.finished') }}/?g_id={{$genre_selected->id}}&sort=sort-date-asc">Сначала старые</a>
                            <a class="dropdown-item" href="{{ route('elements.finished') }}/?g_id={{$genre_selected->id}}&sort=sort-rating-desc">Сначала популярные</a>
                        @else
                            <a class="dropdown-item" href="{{ route('elements.finished') }}/?sort=sort-date-desc">Сначала новые</a>
                            <a class="dropdown-item" href="{{ route('elements.finished') }}/?sort=sort-date-asc">Сначала старые</a>
                            <a class="dropdown-item" href="{{ route('elements.finished') }}/?sort=sort-rating-desc">Сначала популярные</a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-4">
                @include('partials.filter_genre', ['route_filter' => 'elements.finished'])
            </div>
        </div>

        <div class="row">
            <div class="col-5">
                Наименование
            </div>
            <div class="col-2">
                Рейтинг
            </div>
            <div class="col-1">
                Кол-во
            </div>
            <div class="col-2">
                Жанр
            </div>
            <div class="col-2">
                Дата создания
            </div>
        </div>
        @forelse($elements as $element )
        <div class="row">
            <div class="col-5">
                <a href="{{ route('element.show', $element->e_id)  }}">
                    {{ $element->e_name }}
                </a>
            </div>
            <div class="col-2">
                @include('partials.rating_result', ['ratings' => $element->ratings])
            </div>
            <div class="col-1">
                {{$element->count_element_strings}}
            </div>
            <div class="col-2">
                {{$element->g_name}}
            </div>
            <div class="col-2">
                {{ date('H:i  d.m.Y', strtotime($element->e_created_at)) }}
            </div>
        </div>
        @empty
            <div class="row">
                <div class="col-8">
                    <div class="text-center">
                        Здесь пока ничего нет. (
                    </div>
                </div>
            </div>
        @endforelse

        <div class="footer">
            <nav class="Page navigation example">
                <ul class="pagination">
                    {{$elements->links()}}
                </ul>
            </nav>
        </div>

    </div>
@endsection

