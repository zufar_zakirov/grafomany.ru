@extends('layouts.app', ['title' => $element->e_name])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Оконченное произведение @endslot
            @slot('parent') главная @endslot
            @slot('active') <a href="{{route('elements.finished')}}">оконченные произведения</a> @endslot
            @slot('child') {{ $element->e_name }} @endslot
        @endcomponent

        <div class="row">
            <div class="col-12">
                <h1> {{ $element->e_name }} </h1>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                @if($bookmark)
                    <i class="fa fa-bookmark"></i>
                    <a href="{{ route('element.show', $element->e_id ) }}/?page={{$bookmark}}">
                        Перейти на последнию прочитанную страницу
                    </a>
                @endif
            </div>
            <div class="col-6">
                <i class="fa fa-table"></i>
                <a href="{{ route('element.show', $element->e_id ) }}">
                    Перейти на табличный вариант
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                @foreach( $element_strings as $element_string )
                    {{ $element_string->string }}
                    <br />
                @endforeach
            </div>
        </div>
        <div class="row">
            <nav class="Page navigation example">
                <ul class="pagination">
                    {{ $element_strings->links() }}
                </ul>
            </nav>
        </div>




    </div>
@endsection
