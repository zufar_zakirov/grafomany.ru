@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Создание произведения @endslot
            @slot('parent') Главная @endslot
            @slot('active') Произведения @endslot
        @endcomponent

        <form class="form-horizontal form-request-post" action="{{route('element.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            @include('partials.form_element')
        </form>
    </div>
@endsection
