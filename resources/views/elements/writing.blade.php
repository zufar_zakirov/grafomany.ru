@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Пишу произведения @endslot
            @slot('parent') главная @endslot
            @slot('active') пишу произведения @endslot
        @endcomponent



        <div class="row">
            <div class="col-6">
                Наименование
            </div>

            <div class="col-2">
                Кол-во строк
            </div>
            <div class="col-2">
                Жанр
            </div>
            <div class="col-2">
                Дата создания
            </div>
        </div>
        @forelse($elements as $element )
            <div class="row">
                <div class="col-6">
                    <a href="{{ route('element.show', $element->e_id)  }}">
                        {{ $element->e_name }}
                    </a>
                    @if($element->e_group == 1)
                        <sup style="font-size:9px">
                            <i class="fas fa-users"></i>
                        </sup>
                    @endif
                </div>

                <div class="col-2">
                    {{$element->count_element_strings}}
                </div>
                <div class="col-2">
                    <a href="{{route('elements.genre', $element->genre_id)}}">{{$element->g_name}}</a>
                </div>
                <div class="col-2">
                    {{ date('H:i  d.m.Y', strtotime($element->e_created_at)) }}
                </div>
            </div>
        @empty
            <div class="row">
                <div class="col-8">
                    <div class="text-center">
                        Здесь пока ничего нет. (
                    </div>
                </div>
            </div>
        @endforelse

        <div class="footer">
            <nav class="Page navigation example">
                <ul class="pagination">
                    {{$elements->links()}}
                </ul>
            </nav>
        </div>

    </div>
@endsection
