@extends('layouts.app', ['title' => 'Неоконченные произведения'])

@section('content')
    <div class="container">
            <p class="">
                Вы создаете "Закрытое групповое произведение", но не выбрали соавторов.
                <br />
                Вернитесь назад, нажав на кнопку ниже и выберете соавторов или соавтора.
            </p>
            <div class="pull-right">
                <a href="#" onclick="history.back();return false;" class="call-back link-back btn-primary btn">
                    <i class="fas fa-arrow-circle-left" aria-hidden="true"></i>
                    Назад</a>
            </div>
    </div>
@endsection
