@extends('layouts.app', ['title' => $element->e_name])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Оконченное произведение @endslot
            @slot('parent') главная @endslot
            @slot('active') <a href="{{route('elements.finished')}}">оконченные произведения</a> @endslot
            @slot('child') {{ $element->e_name }} @endslot
        @endcomponent

        <div class="row">
            <div class="col-6">
                <h1> {{ $element->e_name }} </h1>
            </div>
            <div class="col-2">
                <b>{{ $element->g_name }}</b>
                <br />
                (жанр)
            </div>
            <div class="col-2">
                <b>{{ $element->u_nic }}</b>
                <br />
                (автор)
            </div>
            <div class="col-2">
                <b>{{ date('H:i - d.m.Y', strtotime( $element->e_created_at )) }}</b>
                <br />
                (дата создания)
            </div>
        </div>

        <div class="row">
            <div class="col-5">
                @if($bookmark)
                    <i class="fa fa-bookmark"></i>
                    <a href="{{ route('element.show', $element->e_id ) }}/?page={{$bookmark}}">
                        Перейти на последнию прочитанную страницу
                    </a>
                @endif
            </div>
            <div class="col-5">
                <i class="fa fa-book"></i>
                @if($bookmark)
                    <a href="{{ route('element.finished.read', $element->e_id )}}/?page={{$bookmark}}">
                        Перейти в читательский режим
                    </a>
                @else
                    <a href="{{ route('element.finished.read', $element->e_id )}}">
                        Перейти в читательский режим
                    </a>
                @endif
            </div>
            <div class="col-2">
                @isset(Auth::user()->id)
                    @include('partials.rating_form', ['action' => 'element', 'ratingable_id' => $element->e_id , 'rating_score' => $element->rating_score])
                @endisset

                @include('partials.rating_result')
            </div>
        </div>

        <div class="row">
            @foreach( $element_strings as $element_string )
                <div class="col-8">
                    {{ $element_string->string }}
                </div>
                <div class="col-2">
                    <a href="{{ route('user.show', $element_string->user_id)  }}">
                        {{ $element_string->u_nic }}
                    </a>
                </div>
                <div class="col-2">
                    {{date('H:i - d.m.Y', strtotime( $element_string->created_at ))}}
                </div>
            @endforeach
        </div>
        <div class="row">
            <nav class="Page navigation example">
                <ul class="pagination">
                    {{ $element_strings->links() }}
                </ul>
            </nav>
        </div>


        <div class="row">
            <div class="col-8">

                <h3>Рецензии</h3>
                @if($count_reviews )
                <a href="{{route('show_reviews_by_element', $element->e_id)}}">
                    Посмотреть все рецензии
                </a>
                @else
                    Здесь пока рецензий нет.
                @endif
            </div>
            <div class="col-4 text-center">

                @if(isset(Auth::user()->id) && $ability_write_review && !$is_author)
                    @if( time() > Auth::user()->ban_time )
                        <i class="fa fa-pen-alt"></i>
                        <a href="{{ route('review.create', $element->e_id ) }}">Написать Рецензию</a>
                    @else
                        У вас бан до
                        <br />
                        <b>{{date('H:i m.d.Y', Auth::user()->ban_time)}}</b>
                    @endif
                @elseif($is_author)
                    Соавторы не могут писать рецензии на свои произведения.
                @endif
            </div>
        </div>


        <div class="row">
            <div class="col-8">
                <h3>Откорректированные тексты</h3>
                @if( $count_corrections )
                    <a href="{{route('show_corrections_by_element', $element->e_id)}}">
                        Посмотреть все откорректированные тексты
                    </a>
                @else
                    Откорректированных текстов нет.
                @endif

            </div>
            <div class="col-4 text-center">
                @if( Auth::check()  && time() > Auth::user()->ban_time )
                    <i class="fa fa-pen-alt"></i>
                    <a href="{{ route('correction.create', $element->e_id ) }}">Откорректировать произведение</a>
                @elseif(isset(Auth::user()->ban_time))
                    У вас бан до
                    <br />
                    <b>{{date('H:i m.d.Y', Auth::user()->ban_time)}}</b>
                @else
                    Откорректировать произведение может только зарегистрированный пользователь
                @endif
            </div>
        </div>
    </div>
@endsection
