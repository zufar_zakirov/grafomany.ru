Оставляем произведение?
<form action="{{route('vote_remove_set')}}" method="post">
    {{csrf_field()}}
    <input type="radio" name="vote_remove" value="1" id="vote_remove_1">
    <label for="vote_remove_1"> Да </label>

    <input type="radio" name="vote_remove" value="0" id="vote_remove_0">
    <label for="vote_remove_0"> Нет </label>

    <input type="submit" value="голосовать">

    <input type="hidden" value="{{$element->e_id}}" name="element_id">
</form>
