<div class="form-group">
    <div class="form-row">
        <label for="correction">Ваша корректорская статья</label>
        <textarea name="text" rows="10" class="form-control" id="correction" data-component>{{ $correction_text }}</textarea>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="published" id="pubRadios1" value="0" checked data-component />
        <label class="form-check-label" for="pubRadios1">
            Сохранить без публикации
        </label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="published" id="pubRadio2" value="1" data-component />
        <label class="form-check-label" for="pubRadio2">
            Сохранить и опубликовать
        </label>
    </div>
</div>
<div class="form-row">
    <button class="btn" type="submit" name="published" value="1">Сохранить</button>
{{--    <button class="btn btn-success mr-4" type="submit" name="unpublished" value="0">Сохранить без публикации</button>--}}
{{--    <button class="btn" type="submit" name="published" value="1">Сохранить и опубликовать</button>--}}
</div>
