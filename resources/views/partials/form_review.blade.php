<div class="form-group">
    <div class="form-row">
        <label for="review">Ваша рецензия на <a href="{{route('element.show', $element_id)}}">{{$element_name}} </a></label>
        <textarea name="text" class="form-control" id="review" data-component>{{$item->review_text ?? ""}}</textarea>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="published" id="pubRadios1" value="0" checked data-component />
        <label class="form-check-label" for="pubRadios1">
            Сохранить без публикации
        </label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="published" id="pubRadio2" value="1" data-component />
        <label class="form-check-label" for="pubRadio2">
            Сохранить и опубликовать
        </label>
    </div>
</div>
<div class="form-group">
    <button class="btn" type="submit" name="published" value="1">Сохранить</button>
</div>
