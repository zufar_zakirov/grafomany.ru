<div class="rating-area" ratingable-type="{{$action}}" link="{{route('rating.set')}}" ratingable-id="{{$ratingable_id}}">
    <?php
    $checked1 = '';
    $checked2 = '';
    $checked3 = '';
    if($rating_score == 3){
        $checked3 = 'checked';
    }elseif($rating_score == 2){
        $checked2 = 'checked';
    }elseif($rating_score == 1){
        $checked1 = 'checked';
    }


    ?>
    <input type="radio" id="star-3" name="rating" value="3" {{$checked3}}>
    <label for="star-3" title="Оценка «3»" value="3"></label>

    <input type="radio" id="star-2" name="rating" value="2" {{$checked2}}>
    <label for="star-2" title="Оценка «2»" value="2"></label>

    <input type="radio" id="star-1" name="rating" value="1" {{$checked1}}>
    <label for="star-1" title="Оценка «1»" value="1"></label>
</div>
