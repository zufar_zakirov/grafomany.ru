@if($noticeCount > 0)
    <a href="{{ route('notifications') }}">
        <i class="circle"><span>{{ $noticeCount }}</span></i>
    </a>
@endif
