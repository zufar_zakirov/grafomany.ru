<div class="form-row">
    <label for="">Статус</label>
    <select class="form-control" name="published">
        @if(isset($genre->id))
            <option value="0" @if($genre->published == 0) selected="" @endif >Не опубликовано</option>
            <option value="1" @if($genre->published == 1) selected="" @endif >Опубликовано</option>
        @else
            <option value="1">Опубликовано</option>
            <option value="0">Не опубликовано</option>
        @endif
    </select>
</div>
<div class="form-row p-3">
    <label for="">Наименование</label>
    <input type="text" class="form-control" name="name" placeholder="Заголовок Жанра" value="{{$genre->name ?? ""}}" required>
</div>

<div class="form-row p-3">
    <label for="">Текст</label>
    <textarea class="form-control" name="text" placeholder="Описание">{{$genre->text ?? ""}}</textarea>
</div>
<div class="form-row p-3 d-none">
    <label for="">Максимально допустимое кол-во символов в строке для этого жанра</label>
    <input type="text" class="form-control" name="letters_for_string" placeholder="150" value="{{$genre->letters_for_string ?? "150"}}" required>
</div>

<div class="form-row p-3">
    <label for="number_lines">Максимально допустимое кол-во строк для этого жанра (0 - неограничено)</label>
    <input type="text" class="form-control" name="number_lines" id="number_lines" placeholder="150" value="{{$genre->number_lines ?? "0"}}" required>
</div>



<div class="form-row p-3">
    <input type="hidden" class="form-control" name="slug" placeholder="автогенерация" value="{{$genre->slug ?? ""}}" readonly>
</div>

<div class="form-row mt-5">
    <input class="btn btn-primary" type="submit" name="some_name" value="Сохранить">
</div>
