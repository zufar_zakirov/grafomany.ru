<div class="dropdown dropdownMenuButton">
    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        @if($genre_selected)
            {{$genre_selected->name}}
        @else
            Жанр
        @endif

    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" href="{{ URL::current() }}">Все жанры</a>
        @isset($false)
            @foreach($genres as $genre)
                <a class="dropdown-item" href="{{ route( $route_filter, $genre->id) }}">{{ $genre->name }}</a>
            @endforeach
        @endif

        @foreach($genres as $genre)
            <a class="dropdown-item" href="{{ route( $route_filter) }}?g_id={{ $genre->id }}">{{ $genre->name }}</a>
        @endforeach

    </div>
</div>
