@if($count_new_message > 0)
    <a href="{{ route('chat') }}">
        <i class="circle"><span>{{ $count_new_message }}</span></i>
    </a>
@endif
