<div class="form-row">
    <label for="">Наименование</label>
    <input type="text" class="form-control" name="name" placeholder="Заголовок" value="{{ $item->name ?? "" }}" required>
</div>

<div class="form-row">
    <label for="">Текст</label>
</div>
<div class="form-row">
<textarea class="form-control" name="text" placeholder="Описание" id="editor">{{ $item->text ?? "" }}</textarea>

</div>

<div class="form-row mt-5">
    <input class="btn btn-primary" type="submit" name="some_name" value="Сохранить">
</div>

<script src="https://cdn.ckeditor.com/ckeditor5/18.0.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
