<div class="form-group">
    <label class="form-check-label" for="genres">Жанр</label>
    <select name="genre_id" required data-component>
        <option value="">-- жанр --</option>
        @foreach ( $genres as $genre )
            <option value="{{ $genre->id }}"> {{ $genre->name }}  </option>
        @endforeach
    </select>
    <input type="hidden" name="slug" value="" />
    <input type="hidden" name="user_id" value="{{Auth::id()}}" data-component />
</div>

<div class="form-group p-3">
    <label for="nameInput">Название</label>
    <input type="text" class="form-control" id="nameInput" name="name" placeholder="Название произведения" required data-component />
    <small id="nameHelp" class="form-text text-muted">Желательно коротко и ёмко.</small>
</div>

<div class="form-group p-3">
    <label for="elements_string">Ваша первая строка</label>
    <input type="text" class="form-control" id="elements_string" name="elements_string" placeholder="Начнем, пожалуй!" required data-component />
</div>


<div class="form-group p-3">
    <label class="form-check-label" for="message">Послание</label>
    <input type="text" class="form-control" id="message" name="message" placeholder="Посыл произведения" data-component />
</div>

<div class="form-group p-3">
    <label class="checkout-group-element js-checkout-group-element" for="checkout-group-element">
        <i class="fas fa-users" aria-hidden="true"></i>
        Закрытое групповое произведение
    </label>
    <input class="js-checkout-group-element" type="checkbox" name="group" id="checkout-group-element" value="0" data-component />

    <div class="select-group-user">
        <div class="row">
            <div class="col-1">
                <i class="fas fa-user-plus" aria-hidden="true"></i>
            </div>
            <div class="col-10">
                <select class="js-data-example-ajax" name="authors[]" multiple="multiple" data-component data-multiples></select>
            </div>
        </div>
    </div>
</div>

<div class="form-group p-3">
    <button type="submit" class="btn btn-primary">
        <i class="fas fa-save" aria-hidden="true"></i>
        Записать</button>
</div>
