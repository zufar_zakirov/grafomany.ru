@if(!empty($ratings))
    <div class="font-weight-bold stars-result" style="color: #dfbe5a">
        @foreach($ratings as $rating)
            @if(3 === (int)$rating->score && $rating->score_count )
                <div class="stars-3">
                    <span class="text-info">{{$rating->score_count}}</span>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            @elseif(2 === (int)$rating->score && $rating->score_count )
                <div class="stars-2">
                    <span class="text-info">{{$rating->score_count}}</span>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            @elseif(1 === (int)$rating->score && $rating->score_count )
                <div class="stars-1">
                    <span class="text-info">{{$rating->score_count}}</span>
                    <i class="fa fa-star"></i>
                </div>
            @endif
        @endforeach
    </div>
@endif
