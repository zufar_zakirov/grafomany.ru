<div class="form-row">
    <textarea class="form-control" name="word" placeholder="Слова. Каждое слово с новой строки" id="add"></textarea>
</div>
<div class="form-group p-3">
    <button type="submit" class="btn btn-primary">
        <i class="fas fa-save" aria-hidden="true"></i>
        Записать</button>
</div>
