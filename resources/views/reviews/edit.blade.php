@extends('layouts.app', ['title' => 'Редактирование рецензии'])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Редактирование рецензии @endslot
            @slot('parent') главная @endslot
            @slot('active') редактирование рецензию @endslot
        @endcomponent

        <div class="row">
            <div class="col-12">
                <h1> Рецензия к
                    <br /><a href="{{route('element.show', $item->element_id)}}">{{ $item->element_name }}</a></h1>
            </div>
        </div>
        <div class="p-5">
            <form class="form-horizontal form-request-post" action="{{ route('review.update', $item->review_id )  }}" method="post">
                {{ csrf_field() }}
                @include('partials.form_review', ['element_name' => $item->element_name, 'element_id' => $item->element_id])
            </form>
        </div>
    </div>
@endsection
