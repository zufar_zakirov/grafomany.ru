@extends('layouts.app')

@section('content')
    <div class="container">

        @component('components.breadcrumb')
            @slot('title') Рецензии от <a href="{{route('user.show', $user->id)}}">{{$user->nic}}</a> @endslot
            @slot('parent') главная @endslot
            @slot('active') рецензии @endslot
        @endcomponent

        <div class="row">
            <div class="col-5">
                Текст рецензии
            </div>
            <div class="col-3">
                Название произведения
            </div>
            <div class="col-2">
                Рейтинг
            </div>
            <div class="col-2">
                Дата рецензии
            </div>
        </div>

        @foreach($items as $item)
            <div class="row">
                <div class="col-5">
                    <a href="{{route('review.show', $item->review_id )}}">
                        {!! mb_substr(   $item->review_text , 0, 70 ) !!}
                        ...
                    </a>
                </div>
                <div class="col-3">
                    <span> Рецензия на
                        <a href="{{route('element.show', $item->element_id)}}"> {{ $item->element_name }} </a>
                    </span>
                </div>
                <div class="col-2">
                    @include('partials.rating_result', ['ratings' => $item->ratings])
                </div>
                <div class="col-2">
                    {{ $item->review_created_at }}
                </div>
            </div>
        @endforeach
    </div>
@endsection
