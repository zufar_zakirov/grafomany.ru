@extends('layouts.app', ['title' => $items[0]->element_name])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Рецензии на {{$items[0]->element_name}} @endslot
            @slot('parent') главная @endslot
            @slot('active')<a href="{{route('elements.finished')}}">оконченные произведения</a> @endslot
            @slot('child')<a href="{{route('element.show', $items[0]->element_id)}}">{{ $items[0]->element_name }}</a>@endslot
            @slot('child2')рецензии@endslot
        @endcomponent

        <div class="row">
            <div class="col-6">
                <h1>Рецензии на<br />
                    {{ $items[0]->element_name }}
                </h1>
            </div>
            <div class="col-2">
                Автор
            </div>
            <div class="col-2">
                Рейтинг
            </div>
            <div class="col-2">
                Дата создания
            </div>
        </div>

        @isset( $items[0]->review_id)
            <div class="row">
                @foreach($items as $item)
                    <div class="col-6">
                        <a href="{{route('review.show', $item->review_id )}}">
                            {!! mb_substr( nl2br($item->review_text) , 0, 70 ) !!}
                            <br />
                            ...
                        </a>
                    </div>
                    <div class="col-2">
                        <a href="{{ route('user.show', $item->user_id ) }}" >
                            {{ $item->user_nic  }}
                        </a>
                    </div>
                    <div class="col-2">
                        @include('partials.rating_result', ['ratings' => $item->ratings])
                    </div>
                    <div class="col-2">
                        {{date('H:i - d.m.Y', strtotime( $item->review_created_at ))}}
                    </div>
                @endforeach
            </div>
            <div class="row">
                <nav class="Page navigation example">
                    <ul class="pagination">
                        {{ $items->links() }}
                    </ul>
                </nav>
            </div>
        @endisset

        @empty($items[0]->review_id)
            <div class="row">
                <div class="col-12">
                    <div class="display-5">
                        Здесь пока рецензий нет.
                    </div>
                </div>
            </div>
        @endisset
    </div>
@endsection
