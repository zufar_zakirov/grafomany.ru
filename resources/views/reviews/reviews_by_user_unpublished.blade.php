@extends('layouts.app', ['title' => 'Мои рецензии в редактуре'])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Мои рецензии в редакции @endslot
            @slot('parent') главная @endslot
            @slot('active') мои рецензии @endslot
        @endcomponent

        <div class="row">
            <div class="col-5">Текст рецензии</div>
            <div class="col-3">Название произведения</div>
            <div class="col-2">Дата рецензии</div>
            <div class="col-2">Управление</div>
        </div>

        @forelse($items as $item)
            <div class="row">
                <div class="col-5">
                    <a href="{{route('review.show', $item->review_id )}}">
                        {!! mb_substr( nl2br(  $item->review_text) , 0, 70 ) !!}
                        <br />
                        ...
                    </a>
                </div>
                <div class="col-3">
                    <spna> Рецензия на
                        <a href="{{route('element.show', $item->element_id)}}"> {{ $item->element_name }} </a>
                    </spna>
                </div>
                <div class="col-2">
                    {{ $item->review_created_at }}
                </div>
                <div class="col-2">
                    <a href="{{route('review.edit', $item->review_id)}}"><i class="mr-1 fa fa-pen"></i> Править</a>
                </div>
            </div>
        @empty
            <div class="row">
                <div class="col-12">
                    В редактуре сейчас нет рецений.
                </div>
            </div>
        @endforelse
    </div>
@endsection
