@extends('layouts.app', ['title' => 'Создание рецензии'])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Добавление рецензии @endslot
            @slot('parent') главная @endslot
            @slot('active') написать рецензию @endslot
        @endcomponent

        <div class="p-5">
            <form class="form-horizontal form-request-post" action="{{ route('review.store', $element->id )  }}" method="post">
                {{ csrf_field() }}
                @include('partials.form_review', ['element_name' => $element->name, 'element_id' => $element->id])
            </form>
        </div>
    </div>
@endsection
