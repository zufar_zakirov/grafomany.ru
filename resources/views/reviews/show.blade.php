@extends('layouts.app', ['title'=> 'Рецензия на '. $review->element_name ])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Рецензия @endslot
            @slot('parent')главная@endslot
            @slot('active')<a href="{{route('elements.finished')}}">оконченные произведения</a> @endslot
            @slot('child') <a href="{{route('element.show', $review->element_id)}}">{{ $review->element_name }}</a> @endslot
            @slot('child2')<a href="{{route('show_reviews_by_element', $review->element_id)}}">рецензии</a>@endslot
        @endcomponent

        @if($review->review_published)
            <div class="row">
                <div class="col-6">
                    <h1>Рецензия на <br />
                        <a href="{{route('element.show', $review->element_id )}}" class="small">
                        {{ $review->element_name }}
                        </a>
                    </h1>
                </div>
                <div class="col-2">
                    <b><a href="{{route('user.show', $review->user_id)}}">{{ $review->user_nic }}</a></b>
                    <br />
                    (автор)
                </div>
                <div class="col-2">
                    <b>Дата</b>
                    <br />
                    {{ $review->review_created_at }}
                </div>
                <div class="col-2">
                    @isset(Auth::user()->id)
                        @include('partials.rating_form', ['action' => 'review',
                                    'ratingable_id' => $review->review_id ,
                                    'rating_score' => $review->rating_score])
                    @endisset

                    @include('partials.rating_result')
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    {!! nl2br($review->review_text) !!}
                </div>
            </div>
        @elseif(0  === (int)$review->review_published AND (int)$review->user_id == (int)Auth::user()->id )
            <div class="row">
                <div class="col-6">
                    <h1 class="text-muted">Рецензия на <br />
                        <a href="{{route('element.show', $review->element_id )}}" class="small">
                            {{ $review->element_name }}
                        </a>
                    </h1>
                    <div class="justify-content-end">
                        <small class="text-muted">Рецензия неопубликована. Её можно
                            <a class="text-danger" href="{{route('review.edit', $review->review_id)}}"> <i class="fa fa-edit"></i> Редактировать</a>
                        </small>
                    </div>
                </div>
                <div class="col-2">
                    <b><a href="{{route('user.show', $review->user_id)}}">{{ $review->user_nic }}</a></b>
                    <br />
                    (автор)
                </div>
                <div class="col-2">
                    <b>Дата</b>
                    <br />
                    {{ $review->review_created_at }}
                </div>
                <div class="col-2">
                    @isset(Auth::user()->id)
                        @include('partials.rating_form', ['action' => 'review',
                                    'ratingable_id' => $review->review_id ,
                                    'rating_score' => $review->rating_score])
                    @endisset

                    @include('partials.rating_result')
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    {!! nl2br($review->review_text) !!}
                </div>
            </div>
        @endif
   </div>
@endsection
