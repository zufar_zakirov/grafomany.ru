@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h3>10 последних неоконченных произведений</h3>
    </div>
    <div class="row">
        <div class="col-5">
            Наименование
        </div>
        <div class="col-3">
            Осталось времени
        </div>

        <div class="col-2">
            Жанр
        </div>
        <div class="col-2">
            Дата создания
        </div>
    </div>
    @foreach($elements as $element )
        <div class="row">
            <div class="col-5">
                <a href="{{ route('element.show', $element->e_id)  }}">
                    {{ $element->e_name }}
                </a>
            </div>
            <div class="col-3">
                {{ $element->remains->number }}
                {{ $element->remains->word }}
            </div>
            <div class="col-2">
                <a href="{{route('elements.genre', $element->genre_id )}}">
                    {{$element->g_name}}
                </a>
            </div>
            <div class="col-2">
                {{ date('d.m.Y H:i', strtotime($element->e_created_at)) }}
            </div>
        </div>
    @endforeach





    <div class="row justify-content-center">
        <h3>10 лучших оконченных произведений</h3>
    </div>
    <div class="row">
        <div class="col-5">
            Наименование
        </div>
        <div class="col-2">
            Рейтинг
        </div>
        <div class="col-1">
            Кол-во
        </div>
        <div class="col-2">
            Жанр
        </div>
        <div class="col-2">
            Дата создания
        </div>
    </div>
    @foreach($elements_finished as $element_f )
        <div class="row">
            <div class="col-5">
                <a href="{{ route('element.show', $element_f->e_id)  }}">
                    {{ $element_f->e_name }}
                </a>
            </div>
            <div class="col-2">
                @include('partials.rating_result', ['ratings' => $element_f->ratings])
            </div>
            <div class="col-1">
                {{$element_f->count_element_strings}}
            </div>
            <div class="col-2">
                <a href="{{route('elements.genre', $element_f->genre_id )}}">
                    {{$element_f->g_name}}
                </a>
            </div>
            <div class="col-2">
                {{ date('H:i  d.m.Y', strtotime($element_f->e_created_at)) }}
            </div>
        </div>
    @endforeach






    <div class="row justify-content-center">
            <h3>10 активных авторов</h3>
    </div>
    <div class="row">
        <div class="col-6">
            Автор (Литературный псевдоним)
        </div>
        <div class="col-4">
            Кол-во написанных строк
        </div>
        <div class="col-2">
            Рейтинг
        </div>
    </div>
    @foreach($users as $user)
        @if( $user_auth_id === $user->id )
            <div class="row my-row">
        @else
            <div class="row">
        @endif
            <div class="col-6">
                <a href="{{route('user.show', $user->id )}}">
                    {{ $user->nic }}

                </a>
            </div>
            <div class="col-4">
                {{ $user->count_element }}
            </div>
            <div class="col-2">
                @include('partials.rating_result', ['ratings' => $user->ratings])
            </div>
        </div>
    @endforeach




</div>
@endsection
