@extends('layouts.app', ['title' => 'Автор ' . $user->nic ])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Профиль автора: <b>{{ $user->nic }}</b> @endslot
            @slot('parent') главная @endslot
            @slot('active') <a href="{{route('user.index')}}">авторы</a> @endslot
            @slot('child') профиль {{ $user->nic }} @endslot
        @endcomponent

        @if( $user_auth_id === $user->id )
        <div class="row">
            <div class="col-2">
                {{ $user->nic }}
            </div>
            <div class="col-5">
                {{ $user->name  }} {{ $user->middle_name  }}  {{ $user->family }}
            </div>
            <div class="col-2">
                {{ $user->birth_day }}
            </div>

            <div class="col-3">
                <a href="{{ route('user.edit', $user->id )  }}"><i class="fa fa-edit"></i>  Изменить</a>
            </div>
        </div>
        @endif

        <div class="row">
            <div class="col-10">
                <ul>
                    <li>
                        @if($count_reviews_by_user)
                            <a href="{{route('show_reviews_by_user', $user->id)}}">Посмотреть все рецензии автора ({{$count_reviews_by_user}})</a>
                        @else
                            У автора нет рецензий.
                        @endif
                    </li>
                    <li>

                        @if($count_corrections_by_user)
                            <a href="{{route('show_corrections_by_user', $user->id)}}">Посмотреть все корректорские статьи автора ({{$count_corrections_by_user}})</a>
                        @else
                            У автора нет корректорских статей
                        @endif
                    </li>
                </ul>
            </div>
            <div class="col-2">
                Ваша оценка автору
                <br/>
                @if( isset(Auth::user()->id) && Auth::user()->id !== $user->id)
                    @include('partials.rating_form', ['action' => 'user',
                        'ratingable_id' => $user->id ,
                        'rating_score' => $user->rating_score])
                @endif


            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <span class="text-muted">Рейтинг автора</span>
                <br/>
                @include('partials.rating_result', ['ratings' => $rating_user])
            </div>
            <div class="col-4">
                <span class="text-muted">Рейтинг критика</span>
                <br/>

                @include('partials.rating_result', ['ratings' => $rating_critic])
            </div>
            <div class="col-4">
                <span class="text-muted">Рейтинг корректора</span>
                <br/>
                @include('partials.rating_result', ['ratings' => $rating_corrector])
            </div>
        </div>

        @if($elements)
            <div class="row">
                <div class="col-12">
                    <h3>Произведения, в которых участвовал автор</h3>
                    <span class="text-success">Зеленым подсвечены оконченные произведения</span>
                    <br />
                    <span class="text-info">В синих можно поучаствовать!</span>
                </div>
            </div>
        @endif
        @foreach($elements as $element)
            @if( $element->e_created_at > Carbon\Carbon::now()->subDays(14))
                <div class="row">
                    <div class="col-8">
                        <b><a href="{{ route('element.show', $element->e_id) }}">{{ $element->e_name }}</a> </b>
                    </div>
                    <div class="col-4">
                        <b>{{ $element->g_name }}</b>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-8">
                        <b><a class="text-success" href="{{ route('element.show', $element->e_id) }}">{{ $element->e_name }}</a> </b>
                    </div>
                    <div class="col-4">
                        <b>{{ $element->g_name }}</b>
                    </div>
                </div>
            @endif
        @endforeach

    </div>
@endsection
