
@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Авторы @endslot
            @slot('parent') главная @endslot
            @slot('active') авторы @endslot
        @endcomponent

        <div class="row justify-content-end">
            <div class="col-6">
                <form method="get" action="{{ route('user.index') }}">
                    <input type="text" name="q" value="{{$q}}" placeholder="поиск">
                    <input type="submit" value="поиск" class="">
                </form>
            </div>
            <div class="col-6">
                <div class="dropdown dropdownMenuButton" >

                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if( $field_sort_selected )
                            {{$field_sort_selected}}
                        @else
                            Сортировка по
                        @endif
                    </button>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('user.index') }}/sort-active">По активности</a>
                        <a class="dropdown-item" href="{{ route('user.index') }}/sort-rating">По рейтингу</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                Автор (Литературный псевдоним)
            </div>
            <div class="col-4">
                Кол-во произведений, в которых участвовал автор
            </div>
            <div class="col-2">
                Рейтинг
            </div>
        </div>
        @foreach($users as $user)
            @if( $user_auth_id === $user->id )
                <div class="row my-row">
            @else
                <div class="row">
            @endif
                <div class="col-6">
                    <a href="{{route('user.show', $user->id )}}">
                        {{ $user->nic }}
                    </a>
                </div>
                <div class="col-4">
                    {{ $user->count_element }}
                </div>
                <div class="col-2">
                    @include('partials.rating_result', ['ratings' => $user->ratings])
                </div>
            </div>
        @endforeach

    </div>
@endsection
