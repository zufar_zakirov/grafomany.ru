<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(isset($title)){{$title}} | @endif Проект Графомания</title>

    <!-- Scripts -->
{{--    <script src="{{ asset('assests/js/app.js') }}" defer></script>--}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('assests/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('assests/css/grid.css') }}" rel="stylesheet">
    <link href="{{ asset('assests/css/fontawesome/all.css') }}" rel="stylesheet">
    <link href="{{ asset('assests/css/styles.css?v=2') }}" rel="stylesheet">

{{--    <link href="https://opensource.qodio.com/selectator/fm.selectator.jquery.css?cb=29" rel="stylesheet">--}}

@if( !isset($jquery) )
    <script src="/assests/js/jquery-3.4.1.min.js"></script>
    <link href="/assests/css/select2.min.css" rel="stylesheet" />
    <script src="/assests/js/select2.full.min.js"></script>
    <script src="/assests/js/js.js?v=2"></script>
    <script src="/assests/js/ru.js"></script>
@else
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/app.js') }}" defer></script>
@endif
</head>
<body class="@if(isset($class_page)){{$class_page}}@endif" >
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <div class="collapse navbar-collapse text-sm-center" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            @if( isset(Auth::user()->id) && time() < Auth::user()->ban_time )
                                <button class="btn btn-light btn-sm pull-right">
                                    <i class="fas fa-ban"></i> Бан до {{date('H:i d.m.Y', Auth::user()->ban_time)}} </button>
                            @else
                                <a href="{{route('element.create')}}" class="btn btn-primary btn-sm pull-right">
                                    <i class="fas fa-pen-fancy"></i> Написать</a>
                            @endif

                        </li>
                    </ul>

                    <ul style="" class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item dropdown dropdownMenuButton">
                                    <a id="" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        <i class="fas fa-person-booth"></i> <span class="caret"> </span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="nav-link" href="{{ route('login') }}">Авторизация</a>
                                        @if (Route::has('register'))
                                            <a class="nav-link" href="{{ route('register') }}">Регистрация</a>
                                        @endif
                                    </div>
                                </li>
                            @else
                                <li class="nav-item dropdown dropdownMenuButton d-flex align-content-center justify-content-start">

                                    <a id="" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->nic }} <span class="caret"> </span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                        @if( Auth::user()->role_id == 3 )
                                            <a class="dropdown-item text-danger" href="{{ route('a_genre.index') }}" >
                                                Управление жанрами
                                            </a>
                                            <a class="dropdown-item text-danger" href="{{ route('a_article.index') }}" >
                                                Управление статьями
                                            </a>
                                            <a class="dropdown-item text-danger" href="{{ route('a_user.index') }}" >
                                                Управление пользователями
                                            </a>
                                            <a class="dropdown-item text-danger" href="{{ route('a_badword.index') }}" >
                                                Управление словами
                                            </a>
                                        @endif

                                        <a class="dropdown-item" href="{{ route('reader') }}" >
                                            <i class="fas fa-book-reader"></i> <span class="menu-item-text">Читаю</span>
                                        </a>
                                        <a class="dropdown-item" href="{{ route('writing') }}" >
                                            <i class="fas fa-pen-fancy"></i> <span class="menu-item-text">Пишу</span>
                                        </a>
                                        <a class="dropdown-item" href="{{ route('elements.group') }}" >
                                            <i class="fas fa-users fa-1x"></i> <span class="menu-item-text">Закрытые произведения</span>
                                        </a>
                                        <a class="dropdown-item" href="{{ route('wrote') }}" >
                                            <i class="fas fa-book"></i> <span class="menu-item-text">Написал</span>
                                        </a>
                                        <a class="dropdown-item" href="{{ route('elements.draft') }}" >
                                            <i class="fas fa-trash-alt"></i> <span class="menu-item-text">Черновики</span>
                                        </a>

                                        <a class="dropdown-item" href="{{ route('show_reviews_by_user', Auth::user()->id ) }}" >
                                            <i class="fas fa-comment"></i> <span class="menu-item-text">Мои рецензии</span>
                                        </a>
                                        <a class="dropdown-item text-success" href="{{ route('reviews_in_work') }}" >
                                            <i class="far fa-comment"></i> <span class="menu-item-text">Мои рецензии в редакции</span>
                                        </a>

                                        <a class="dropdown-item" href="{{ route('show_corrections_by_user', Auth::user()->id) }}" >
                                            <i class="fas fa-sticky-note"></i> <span class="menu-item-text">Мои откорректированные тексты</span>
                                        </a>

                                        <a class="dropdown-item text-success" href="{{ route('corrections_in_work') }}" >
                                            <i class="fas fa-edit"></i> <span class="menu-item-text">Мои редактируемые тексты</span>
                                        </a>

                                        <a class="dropdown-item" href="{{ route('user.show', Auth::user()->id ) }}" >
                                            <i class="fas fa-user-circle"></i> <span class="menu-item-text">Профиль</span>
                                        </a>
                                            <a class="dropdown-item" href="{{ route('notifications' ) }}" >
                                                <i class="fas fa-bell"></i> <span class="menu-item-text">Уведомления</span>
                                            </a>
                                            <a class="dropdown-item" href="/chat" >
                                                <i class="fas fa-mail-bulk"></i> <span class="menu-item-text">Чаты</span>
                                            </a>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            Выйти
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                                <li>
                                    @include('partials.notice_count')
                                    @include('partials.count_new_message')
                                </li>
                            @endguest
                        </ul>
                </div>
            </div>
        </nav>

        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="/assests/images/logo.jpeg" width="230">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse text-sm-center" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href=" {{ route('genres') }}">Жанры</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href=" {{ route('elements') }}">Неоконченные произведения</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href=" {{ route('elements.finished') }}">Оконченные произведения</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href=" {{ route('user.index') }}">Авторы</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href=" {{ route('article.index') }}">База знаний</a>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->

                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        © Все права защищены.
                        <hr />
                        Все вопросы и пожелания можно отправить на email: <a href="mailto:mail@grafomany.ru">mail@grafomany.ru</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
{{--    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>--}}
    @stack('scripts')
</body>
</html>
