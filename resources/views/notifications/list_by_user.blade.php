@extends('layouts.app', ['title' => 'Уведомления'])

@section('content')
    <div class="container">
    @component('components.breadcrumb')
        @slot('title') Уведомления @endslot
        @slot('parent') главная @endslot
        @slot('active') уведомления @endslot
    @endcomponent


    @forelse($notices as $notice)

    <div class="row {{$notice->read}}">
        <div class="col-2">
            <h6>
                {{ $notice->data['title'] }}
            </h6>
        </div>
        <div class="col-7">
            @if( 'ban'  === $notice->data['type'] )
                Вас забанил админ до {{ $notice->data['ban'] ? 'навсегда' : $notice->data['ban_time'] }}
                <br />
                Сообщение от администратора:
                <br />
                {{ $notice->data['message'] }}
                <br />
                <small>
                По всем вопросам можете связаться с администратором по email: mail@grafomany.ru
                </small>
            @elseif( 'invite' === $notice->data['type'] )

                Автор под псевдонимом <b>{{ $notice->data['user_from']['nic'] }}</b> пригласил вас в закрытое групповое произведение:
                <br />
                <a href="{{ route('element.show', $notice->data['element']['id']) }}">
                    {{ $notice->data['element']['name'] }}
                </a>
            @endif
        </div>
        <div class="col-2">
            {{$notice->created_at}}
        </div>
        <div class="col-1">
            <form action="{{route('notifications_remove')}}" method="post">
{{--                <input type="hidden" name="_method" value="DELETE">--}}
                {{csrf_field()}}
                <input type="hidden" name="noticeId" value="{{$notice->id}}">
                <button type="submit" class="btn"  id="submit_delete_notice">
                    <i class="fas fa-trash-alt"></i>
                </button>
            </form>
        </div>
    </div>
    @empty
        <div class="row">
            <div class="col-12">
                <div class="text-center">
                    У вас пока нет уведомлений.
                </div>
            </div>
        </div>
    @endforelse
</div>
@endsection
