<div class="row">
    <div class="col-12">
        <h2>{{$title}}</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">{{$parent}}</a></li>
            <li class="breadcrumb-item active">{{$active}}</li>
            @if( isset($child) )
            <li class="breadcrumb-item">{{$child}}</li>
            @endif
            @if( isset($child2) )
                <li class="breadcrumb-item">{{$child2}}</li>
            @endif

        </ol>
    </div>
</div>
