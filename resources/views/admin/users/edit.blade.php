@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Изменение профиля @endslot
            @slot('parent') главная @endslot
            @slot('active') изменение пользователя @endslot
        @endcomponent

        <form class="" action="{{ route('user.update', $user->id )  }}" method="post">
            <div class="row text-center justify-content-center">
                {{ csrf_field() }}
                <div class="col-6">
                    <div class="form-group">
                        <label for="nic">Литературный псевдоним</label>
                        <input value="{{ $user->nic }}" class="form-control" name="nic" placeholder="Nic" type="text" id="nic">
                    </div>
                    <div class="form-group  mt-4">
                        <label for="email">Email</label>
                        <input value="{{ $user->email }}" class="form-control" name="email" placeholder="Email" type="email" id="email">
                    </div>
                    <div class="form-group  mt-4">
                        <label for="birth_day">Дата рождения</label>
                        <input value="{{ $user->birth_day  }}" class="form-control" type="date"  name="birth_day" id="birth_day">
                    </div>

                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="name">Имя</label>
                        <input value="{{ $user->name  }}" class="form-control" type="text" placeholder="Имя" name="name" id="name">
                    </div>

                    <div class="form-group  mt-4">
                        <label for="family">Фамилия</label>
                        <input value="{{ $user->family  }}" class="form-control" type="text" placeholder="Фамилия" name="family" id="family">
                    </div>

                    <div class="form-group  mt-4">
                        <label for="middle_name">Отчество</label>
                        <input value="{{ $user->middle_name  }}" class="form-control" type="text" placeholder="Отчество" name="middle_name" id="middle_name">
                    </div>

                </div>



                <div class="form-row mt-4">
                    <input type="submit" value="Сохранить" class="form-control btn btn-success" />
                </div>
            </div>
        </form>
    </div>
@endsection
