@extends('layouts.app', ['title' => 'Управление пользователями'])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Управление пользователями @endslot
            @slot('parent') главная @endslot
            @slot('active') пользователи @endslot
        @endcomponent

        <div class="row justify-content-start">
            <div class="col-4">
                <form method="get" action="{{ route('a_user.index') }}">
                    <input type="text" name="q" value="{{ isset($q) ? $q : ''}}" placeholder="поиск">
                    <input type="submit" value="поиск" class="">
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-2">Nic</div>
            <div class="col-3">ФИО</div>
            <div class="col-2">Забанен до</div>
            <div class="col-4">Управление</div>
        </div>
        @forelse($items as $item )
            <div class="row">
                <div class="col-2">
                    {{$item->nic}}
                </div>
                <div class="col-3">
                    {{$item->family}} {{$item->name}} {{$item->middle_name}}
                </div>
                <div class="col-2">
                    <span class="ban_until" id="ban_until_{{$item->id}}">
                    @if( 1 == $item->ban )
                        навсегда
                    @elseif( time() < $item->ban_time )
                        {{ date('H:i d-m-Y', $item->ban_time) }}
                    @endif
                    </span>
                </div>
                <div class="col-2">
                    <a href="{{ route('a_user.edit',  $item->id ) }}"> <i class="fas fa-edit"></i> Изменить</a>
                </div>
                <div class="col-2 d-flex justify-content-between">
                    <button href="#" class="js-btnBan btn-sm btn-danger" attrId="{{$item->id}}" attrNic="{{$item->nic}}">
                        <i class="fas fa-ban"></i> Бан
                    </button>
                    <form onsubmit="if(confirm('Удалить?')){return true}else{return false}" action="{{route('a_user.destroy', $item)}}" method="post">
                        <input type="hidden" name="_method" value="DELETE">
                        {{csrf_field()}}
                        <button type="submit" class="btn"><i class="fas fa-trash"></i></button>
                    </form>
                </div>
            </div>
        @empty
            <div class="row">
                <div class="col-8">
                    <div class="text-center">
                        Здесь пока ничего нет. (
                    </div>
                </div>
            </div>
        @endforelse

        <div class="footer">
            <nav class="Page navigation example">
                <ul class="pagination">{{$items->links()}}
                </ul>
            </nav>
        </div>

        <div class="modal modal-bun" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Баним пользователя</h5>
                        <button type="button" class="close js-btnClose" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            Пользователь:
                            <br />
                            <span class="user_nic_insert font-weight-bold"></span>
                        </p>
                        <p>На сколько будем блокировать?</p>
                        <input type="hidden" value="" name="user_id" class="insert-user_id" id="insert-user_id">
                        <input type="hidden" value="{{route('a_user.bun')}}" name="link_user_ban" class="link_user_ban" id="link_user_ban">
                        <select class="ban_time" name="ban_time" id="ban_time" required>
                            <option value="">Выбрать</option>
                            <option value="day0">Разбанить</option>
                            <option value="day7">7 дней</option>
                            <option value="day30">30 дней</option>
                            <option value="day60">60 дней</option>
                            <option value="permanently">Навсегда</option>
                        </select>
                        <p>
                            Сообщение пользователю:
                        </p>
                        <textarea name="message" id="js-message"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary jsBan">Отправить</button>
                        <button type="button" class="btn btn-secondary js-btnClose" data-dismiss="modal">Отменить и закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
