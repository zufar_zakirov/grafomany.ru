@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Статьи @endslot
            @slot('parent') главная @endslot
            @slot('active') статьи @endslot
        @endcomponent
        <div class="row justify-content-lg-end">
                <a href="{{route('a_article.create')}}" class="btn btn-primary pull-right">
                    <i class="fas fa-folder-plus"></i> Добавить статью
                </a>
        </div>
        <div class="row">
            <div class="col-5">
                Наименование
            </div>
            <div class="col-4">
                Начало текста
            </div>
            <div class="col-3">
                Управление
            </div>
        </div>
        @forelse($items as $item )
            <div class="row">
                <div class="col-5">
                    {{$item->name}}
                </div>
                <div class="col-4">
                    {{ strip_tags( mb_substr($item->text, 0, 70), '<br>' ) }}
                    <br />
                    ...
{{--                    {!! mb_substr($item->text, 0, 70) !!}--}}
                </div>
                <div class="col-2">
                    <a href="{{ route('a_article.edit',  $item->id ) }}"> <i class="fas fa-edit"></i> Изменить</a>
                </div>
                <div class="col-1">
                    <form onsubmit="if(confirm('Удалить?')){return true}else{return false}" action="{{route('a_article.destroy', $item)}}" method="post">
                        <input type="hidden" name="_method" value="DELETE">
                        {{csrf_field()}}
                        <button type="submit" class="btn"><i class="fas fa-trash"></i></button>
                    </form>
                </div>
            </div>
        @empty
            <div class="row">
                <div class="col-8">
                    <div class="text-center">
                        Здесь пока ничего нет. (
                    </div>
                </div>
            </div>
        @endforelse

        <div class="footer">
            <nav class="Page navigation example">
                <ul class="pagination">{{$items->links()}}
                </ul>
            </nav>
        </div>

    </div>
@endsection
