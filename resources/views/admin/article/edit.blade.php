@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Редактирование статьи @endslot
            @slot('parent') главная @endslot
            @slot('active') статьи @endslot
        @endcomponent

        <form class="form-horizontal" action="{{ route('a_article.update', $item ?? '' ) }}" method="post" enctype="multipart/form-data">
{{--            <input type="hidden" name="_method" value="put">--}}
            {{csrf_field()}}
            {{-- Form include --}}
            @include('partials.form_article')
        </form>
    </div>


@endsection
