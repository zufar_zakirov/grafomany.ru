@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Создание статьи @endslot
            @slot('parent') главная @endslot
            @slot('active') статьи @endslot
        @endcomponent

        <form class="form-horizontal" action="{{route('a_article.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            {{-- Form include --}}
            @include('partials.form_article')
        </form>
    </div>
@endsection
