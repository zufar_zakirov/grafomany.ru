@extends('layouts.app')
@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Список слов @endslot
            @slot('parent') Главная @endslot
            @slot('active') Слова @endslot
        @endcomponent
        <div class="row justify-content-lg-end">
            <a href="{{route('a_badword.create')}}" class="btn btn-primary pull-right">
                <i class="fas fa-folder-plus"></i> Добавить слова
            </a>
        </div>
        <table class="table" style="width:400px">
            <thead>
                <th class="col">
                    Слова
                </th>
                <th class="col">
                    Управление
                </th>
            </thead>
            <tbody>
                @forelse($badwords as $item)
                    <tr>
                        <td>
                            {{$item->word}}
                        </td>
                        <td>
                            <form onsubmit="if(confirm('Удалить?')){return true}else{return false}" action="{{route('a_badword.destroy', $item->id)}}" method="post">
                                <input type="hidden" name="badword" value="{{$item->id}}">
                                <input type="hidden" name="_method" value="POST">
                                {{csrf_field()}}
                                <button type="submit" class="btn"><i class="fas fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>
                            <div class="text-center">
                                Здесь пока ничего нет. (
                            </div>
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>

        <div class="footer">
            <nav class="Page navigation example">
                <ul class="pagination">{{$badwords->links()}}
                </ul>
            </nav>
        </div>

    </div>
@endsection
