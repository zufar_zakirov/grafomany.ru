@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Добавление слов @endslot
            @slot('parent') главная @endslot
            @slot('active') слова @endslot
        @endcomponent

        <form class="form-horizontal" action="{{route('a_badword.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            @include('partials.form_badword')
        </form>
    </div>
@endsection
