@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Редактирование жанра @endslot
            @slot('parent') Главная @endslot
            @slot('active') Жанры @endslot
        @endcomponent

        <form class="form-horizontal" action="{{ route('a_genre.update', $genre ?? '' ) }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="put">
            {{csrf_field()}}
            {{-- Form include --}}
            @include('partials.form_genre')
        </form>
    </div>


@endsection
