@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Создание жанра @endslot
            @slot('parent') Главная @endslot
            @slot('active') Категории @endslot
        @endcomponent

        <form class="form-horizontal" action="{{route('a_genre.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            {{-- Form include --}}
            @include('partials.form_genre')

        </form>
    </div>


@endsection
