@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') База знаний @endslot
            @slot('parent') главная @endslot
            @slot('active') база знаний @endslot
        @endcomponent

        <div class="row">
            <div class="col-12">
                Наименование
            </div>
        </div>
        @forelse($items as $item )
            <div class="row">
                <div class="col-12">
                    <a href="{{route('article.show', $item->id )}}">
                    {{$item->name}}
                    </a>
                </div>
            </div>
        @empty
            <div class="row">
                <div class="col-8">
                    <div class="text-center">
                        Здесь пока ничего нет. (
                    </div>
                </div>
            </div>
        @endforelse

        <div class="footer">
            <nav class="Page navigation example">
                <ul class="pagination">{{$items->links()}}
                </ul>
            </nav>
        </div>

    </div>
@endsection
