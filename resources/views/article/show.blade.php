@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Статья @endslot
            @slot('parent') главная @endslot
            @slot('active') статья @endslot
        @endcomponent

        <div class="row">
            <div class="col-12">
                <h1>{{ $item->name }}</h1>
            </div>
            <div class="col-12">
                <div class="article">
                    {!! $item->text !!}
                </div>
            </div>
        </div>
    </div>
@endsection
