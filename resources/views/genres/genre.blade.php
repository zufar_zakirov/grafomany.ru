@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Жанры @endslot
            @slot('parent') главная @endslot
            @slot('active') жанры @endslot
        @endcomponent
        <div class="row justify-content-lg-end">
                <a href="{{route('genre.create')}}" class="btn btn-primary pull-right">
                    <i class="fas fa-folder-plus"></i> Добавить жанр
                </a>
        </div>
        <div class="row">
            <div class="col-5">
                Наименование
            </div>
            <div class="col-4">
                Первая строка
            </div>
            <div class="col-3">
                Кол-во строк
            </div>
        </div>
        @forelse($genres as $genre )
            <div class="row">
                <div class="col-5">
                    {{$genre->name}}
                </div>
                <div class="col-4">
                    {{$genre->text}}
                </div>
                <div class="col-2">
                    <a href="{{ route('genre.edit',  $genre->id ) }}"> <i class="fas fa-edit"></i> Изменить</a>
                </div>
                <div class="col-1">
                    <form onsubmit="if(confirm('Удалить?')){return true}else{return false}" action="{{route('genre.destroy', $genre)}}" method="post">
                        <input type="hidden" name="_method" value="DELETE">
                        {{csrf_field()}}
                        <button type="submit" class="btn"><i class="fas fa-trash"></i></button>
                    </form>
                </div>
            </div>
        @empty
            <div class="row">
                <div class="col-8">
                    <div class="text-center">
                        Здесь пока ничего нет. (
                    </div>
                </div>
            </div>
        @endforelse

        <div class="footer">
            <nav class="Page navigation example">
                <ul class="pagination">{{$genres->links()}}
                </ul>
            </nav>
        </div>

    </div>
@endsection
