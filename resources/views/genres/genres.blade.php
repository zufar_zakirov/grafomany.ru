@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Жанры @endslot
            @slot('parent') главная @endslot
            @slot('active') жанры @endslot
        @endcomponent
        <div class="row">
            <div class="col-6">
                Наименование
            </div>
            <div class="col-3">
                Кол-во неоконченных произведений
            </div>
            <div class="col-3">
                Кол-во оконченных произведений
            </div>
        </div>
        @forelse($genres as $genre )
            <div class="row">
                <div class="col-6">
                    {{$genre->name}}
                </div>

                <div class="col-3">
                    <a href="{{route('elements.genre', $genre->id )}}">
                        {{$genre->count_elements}}
                    </a>
                </div>

                <div class="col-3">
                    <a href="{{route('elements.finished.genre', $genre->id )}}">
                        {{$genre->count_elements_finished}}
                    </a>
                </div>
            </div>
        @empty
            <div class="row">
                <div class="col-8">
                    <div class="text-center">
                        Здесь пока ничего нет. (
                    </div>
                </div>
            </div>
        @endforelse

        <div class="footer">
            <nav class="Page navigation example">
                <ul class="pagination">{{$genres->links()}}
                </ul>
            </nav>
        </div>

    </div>
@endsection
