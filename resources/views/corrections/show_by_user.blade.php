@extends('layouts.app', ['title' => 'Откорректированные тексты '. $user->nic] )

@section('content')
    <div class="container">

        @component('components.breadcrumb')
            @slot('title') Откорректированные тексты от <a href="{{route('user.show', $user->id)}}">
                    {{$user->nic}}</a> @endslot
            @slot('parent') главная @endslot
            @slot('active') <a href="{{route('user.index')}}">авторы</a> @endslot
            @slot('child')  <a href="{{route('user.show', $user->id)}}">
                    профиль {{ $user->nic }}
                </a>  @endslot
            @slot('child2') откорректированные тексты @endslot
        @endcomponent
        @if(!$items->isEmpty())
            <div class="row">
                <div class="col-5">
                    Откорректированные тексты
                </div>
                <div class="col-3">
                    Название произведения
                </div>
                <div class="col-2">
                    Рейтинг
                </div>
                <div class="col-2">
                    Дата текста
                </div>
            </div>

            @foreach($items as $item)
                <div class="row">
                    <div class="col-5">
                        <a href="{{route('correction.show', $item->correction_id )}}">
                            {!! mb_substr( nl2br(  $item->correction_text) , 0, 70 ) !!}
                            <br />
                            ...
                        </a>
                    </div>
                    <div class="col-3">
                        <span class="text-muted"> Откорректированный текст произведения <br />
                            <a href="{{route('element.show', $item->element_id)}}"> {{ $item->element_name }} </a>
                        </span>
                    </div>
                    <div class="col-2">
                        @include('partials.rating_result', ['ratings' => $item->ratings])
                    </div>
                    <div class="col-2">
                        {{ $item->correction_created_at }}
                    </div>
                </div>
            @endforeach
                <div class="row">
                    <nav class="Page navigation example">
                        <ul class="pagination">
                            {{ $items->links() }}
                        </ul>
                    </nav>
                </div>
        @else
            <div class="row">
                <div class="col-12">
                    Здесь пока пусто.
                </div>
            </div>
        @endif
    </div>
@endsection
