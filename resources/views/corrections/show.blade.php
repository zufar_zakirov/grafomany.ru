@extends('layouts.app', ['title'=> 'Откорректированный текст '. $correction->element_name ])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Откорректированный текст @endslot
            @slot('parent') главная @endslot
            @slot('active')<a href="{{route('elements.finished')}}"> оконченные произведения</a> @endslot
            @slot('child') <a href="{{route('element.show', $correction->element_id)}}">
                    {{ $correction->element_name }}
                </a> @endslot
                @slot('child2')<a href="{{route('show_corrections_by_element', $correction->element_id)}}">откорректированные тексты</a>@endslot
        @endcomponent

        @if($correction->correction_published)
            <div class="row">
                <div class="col-6">
                    <h1> <small class="text-muted">Откорректированный текст на</small>
                        <br />
                        <a href="{{route('element.show', $correction->element_id)}}">
                        {{ $correction->element_name }}
                        </a>
                    </h1>
                </div>
                <div class="col-2">
                    <b><a href="{{route('user.show', $correction->user_id )}}">{{ $correction->user_nic }}</a></b>
                    <br />
                    (автор)
                </div>
                <div class="col-2">
                    <b>Дата</b>
                    <br />
                    {{ $correction->correction_created_at }}
                </div>
                <div class="col-2">
                    @isset(Auth::user()->id)
                        @include('partials.rating_form', ['action' => 'correction',
                                    'ratingable_id' => $correction->correction_id ,
                                    'rating_score' => $correction->rating_score])
                    @endisset

                    @include('partials.rating_result')
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    {!! nl2br($correction->correction_text) !!}
                </div>
            </div>
        @elseif( 0 === (int)$correction->correction_published AND (int)$correction->user_id === (int)Auth::user()->id )
            <div class="row">
                <div class="col-6">
                    <h1 class="text-muted">Корректорское произведение на <br />
                        <a href="{{route('element.show', $correction->element_id )}}" class="small">
                            {{ $correction->element_name }}
                        </a>
                    </h1>
                    <div class="justify-content-end">
                        <small class="text-muted">Корректорская статья неопубликована. Её можно
                            <a class="text-danger" href="{{route('correction.edit', $correction->correction_id)}}"> <i class="fa fa-edit"></i> Редактировать</a>
                        </small>
                    </div>
                </div>
                <div class="col-2">
                    <b>{{ $correction->genre_name }}</b>
                    <br />
                    (жанр)
                </div>
                <div class="col-2">
                    <b><a href="{{route('user.show', $correction->user_id)}}">{{ $correction->user_nic }}</a></b>
                    <br />
                    (автор)
                </div>
                <div class="col-2">
                    <b>Дата</b>
                    <br />
                    {{ $correction->correction_created_at }}
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    {!! nl2br($correction->correction_text) !!}
                </div>
            </div>
        @endif
    </div>
@endsection
