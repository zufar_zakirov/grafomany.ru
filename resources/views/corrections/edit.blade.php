@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Редактирование корректировки @endslot
            @slot('parent') главная @endslot
            @slot('active') редактируем корректорскую статью @endslot
        @endcomponent

        <div class="row">
            <p>
                Вы корректируете произведение:
                <br />
                <a class="text-success card-text" href="{{route('element.show', $item->element_id)}}"> {{ $item->element_name }}</a>
            </p>
        </div>

        <div class="p-3">
            <form class="form-horizontal form-request-post" action="{{ route('correction.update', $item->correction_id )  }}" method="post">
                {{ csrf_field() }}
                @include('partials.form_correction')
            </form>
        </div>
    </div>
@endsection
