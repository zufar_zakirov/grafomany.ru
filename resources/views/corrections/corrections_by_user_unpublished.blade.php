@extends('layouts.app', ['title' => 'Мои редактируемые тексты'])

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Мои редактируемые тексты @endslot
            @slot('parent') главная @endslot
            @slot('active') мои редактируемые тексты @endslot
        @endcomponent

        <div class="row">
            <div class="col-5">
                Редактируемые тексты
            </div>
            <div class="col-3">
                Название произведения
            </div>
            <div class="col-2">
                Дата рецензии
            </div>
            <div class="col-2">
                Управление
            </div>
        </div>

        @forelse($items as $item)
            <div class="row">
                <div class="col-5">
                    <a href="{{route('correction.show', $item->correction_id )}}">
                        {!! mb_substr( nl2br(  $item->correction_text) , 0, 70 ) !!}
                        <br />
                        ...
                    </a>
                </div>
                <div class="col-3">
                    <spna> Рецензия на
                        <a href="{{route('element.show', $item->element_id)}}"> {{ $item->element_name }} </a>
                    </spna>
                </div>
                <div class="col-2">
                    {{ $item->correction_created_at }}
                </div>
                <div class="col-2">
                    <a href="{{route('correction.edit', $item->correction_id)}}"><i class="mr-1 fa fa-pen"></i> Править</a>
                </div>
            </div>
        @empty
            <div class="row">
                <div class="col-12">
                    Редактируемые тексты отсутствует.
                </div>
            </div>
        @endforelse
    </div>
@endsection
