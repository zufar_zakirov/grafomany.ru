@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.breadcrumb')
            @slot('title') Добавление корректировки @endslot
            @slot('parent') главная @endslot
            @slot('active') написать корректорскую статью @endslot
        @endcomponent

        <div class="row">
            <p>Вы корректируете произведение: <br />
                 <a class="text-success card-text" href="{{route('element.show', $element->id)}}"> {{ $element->name }}</a>
            </p>
        </div>
        <div class="p-3">
            <form class="form-horizontal form-request-post" action="{{ route('correction.store', $element->id )  }}" method="post">
                {{ csrf_field() }}
                @include('partials.form_correction', ['route' =>  route('correction.store', $element->id ) ])
            </form>
        </div>
    </div>
@endsection
