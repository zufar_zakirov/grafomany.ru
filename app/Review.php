<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [ 'id' , 'element_id' , 'user_id', 'text', 'published', 'created_at', 'updated_at' ];
}
