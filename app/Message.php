<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [ 'id' , 'user_id', 'text', 'chat_id', 'created_at', 'updated_at' ];

    protected $guarded = [];
}
