<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable = [ 'id' , 'user_id', 'created_at', 'updated_at' ];

    public function users()
    {
        return $this->belongsToMany('\App\User', 'user_chat');
    }

}
