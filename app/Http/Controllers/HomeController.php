<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Elements\ElementsController;

use App\Http\Controllers\User\UserController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ElementsController = new ElementsController;
        $elements_finished = $ElementsController->get_elements_finished(10, '', 'sort-rating-desc');
        $elements = $ElementsController->get_elements(null, 10);

        $AuthorsController = new UserController;

        if(isset(Auth::user()->id)){
            $user_id = Auth::user()->id;
        }else{
            $user_id = 0;
        }
        $authors = $AuthorsController->get_authors('sort-active', 10);


        return view('home',
            [
                'elements_finished' => $elements_finished,
                'elements' => $elements,
                'users' => $authors,
                'user_auth_id' => $user_id
            ]
        );
    }
}
