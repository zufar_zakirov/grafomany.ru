<?php

namespace App\Http\Controllers;

use App\Genre;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('genres.genres',
            [
                'genres' => Genre::paginate(20)
            ]
        );
    }


    /**
     * Display a listing of the resource.
     */
    public function genres()
    {
        $genres = DB::table('genres')
            ->leftJoin('elements AS e2', function($join){
                $join->on('e2.genre_id', '=', 'genres.id')
                    ->where(function ($query){
                        $query->where( 'e2.created_at', '<', Carbon::now()->subDays(14) )
                            ->orWhere('e2.finished', '=', 1, 'or');
                    });

            })
            ->leftJoin('elements AS e1', function($join){
                $join->on('e1.genre_id', '=', 'genres.id')
                    ->where( 'e1.created_at', '>', Carbon::now()->subDays(14) );
            })
            ->select('genres.id', 'genres.name', 'genres.text', 'genres.title_meta' , 'genres.desc_meta')
            ->selectRaw('COUNT(DISTINCT e1.id) AS count_elements, COUNT(DISTINCT e2.id) AS count_elements_finished')
            ->groupBy('genres.id')
            ->orderBy('name')
            ->paginate(20);


        return view('genres.genres',
            [
                'genres' => $genres
            ]
        );
    }



    /**
     * Return Genres.
     */
    public function get_genres()
    {
        return Genre::all();
    }


    /**
     * Display a listing of the resource.
     * @param Genre $genre
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function genre(Genre $genre)
    {
        $count_elements_by_genre = DB::table('elements')
            ->where('genre_id', '=', $genre->id)
            ->first();
        //dd($count_elements_by_genre);
        return view('genres.genres',
            [
                'genres' => $genre ,
                'count_elements_by_genre' => $count_elements_by_genre
            ]
        );
    }

    /**
     * Return Genre
     * @param Genre $genre
     */
    public function get_genre(Genre $genre)
    {
//        $genre = Genre::where('id', '=', )
//        return ;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.genre.create_genre' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $genre = Genre::create($request->all());
        return redirect()->route('genre.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function show(Genre $genre)
    {
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function edit(Genre $genre)
    {
           return view('admin.genre.edit_genre',[
               'genre' => $genre
           ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Genre $genre)
    {
        $genre->update($request->except('slug'));
        return redirect()->route('genre.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Genre $genre)
    {
        $genre->delete();
        return redirect()->route('genre.index');
    }


    /**
     * Get Number Lines
     *
     * @param  \App\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function get_number_lines_by_id($id)
    {
        return Genre::select('number_lines')->where('id', '=', $id)->first();
    }
}
