<?php

namespace App\Http\Controllers;

use App\BadWord;
use \Wkhooy\ObsceneCensorRus;



class BadWordController
{
    public function getAll()
    {
        return BadWord::all();
    }

    public function isBadWord($str)
    {

        return !ObsceneCensorRus::isAllowed($str);
//        foreach($this->getAll() as $dataWord){
//            $badWord = "|^". $dataWord->word ."$|";
//            if(preg_match_all($badWord, $str) !== 0){
//                return $badWord;
//            }
//        }
//        return false;
    }
}
