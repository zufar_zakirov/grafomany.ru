<?php

namespace App\Http\Controllers;

use Auth;
use App\Rating;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Collection;

class RatingController extends Controller
{

    public $critic = 'review';
    public $corrector = 'correction';


    /**
     * Preparation data and sending for recording
     *
     * @param Request $request
     * @return void
     */
    public function set_rating(Request $request)
    {
        $score = $request->input('score');
        $ratingableId = $request->input('ratingableId');
        $ratingableType = $request->input('ratingableType');
        $userId = Auth::user()->id;

        $arrayForDb = [
            'score' => $score,
            'ratingable_id'  => $ratingableId,
            'ratingable_type' => $ratingableType,
            'user_id' => $userId
        ];


        $resRating = $this->get_rating_by_user($ratingableId,  $ratingableType, $userId);

        if(empty($resRating)){
            $res = $this->store($arrayForDb);
        }else{
            $res = $this->update($arrayForDb, $resRating->id);
        }
        //echo json_decode( $resRating );

        //echo $resRating->toJson();
    }



    /**
     * Get the User's rating to check for records.
     * Получение рейтинга пользователя для проверки на наличие записей.
     *
     * @param $ratingableId
     * @param $ratingableType
     * @param $userId
     * @return Collection
     */
    public function get_rating_by_user($ratingableId,  $ratingableType, $userId)
    {
        $result = Rating::where('user_id', $userId)
            ->where('ratingable_id', $ratingableId)
            ->where('ratingable_type', $ratingableType)
            ->first();
        return $result;
    }



    /**
     * Get rating for Critic-User
     *
     * @param $userCriticId
     * @return \Illuminate\Support\Collection
     */
    public function get_ratings_by_critic($userCriticId)
    {
        return DB::table('ratings AS rtg')
            ->select('rtg.score AS score')
            ->selectRaw('COUNT(rtg.id) AS  score_count')
            ->leftJoin('reviews AS rw', 'rw.id', '=', 'rtg.ratingable_id')
            ->where('rtg.ratingable_type', '=', $this->critic)
            ->where('rw.user_id', '=', $userCriticId)
            ->groupBy('score')
            ->orderBy('score', 'DESC')
            ->get()->toArray();
    }


    /**
     * Get rating for Corrector-User
     *
     * @param $userCriticId
     * @return \Illuminate\Support\Collection
     */
    public function get_ratings_by_corrector($userCriticId)
    {
        return DB::table('ratings AS rtg')
            ->select('rtg.score AS score')
            ->selectRaw('COUNT(rtg.id) AS  score_count')
            ->leftJoin('corrections AS c', 'c.id', '=', 'rtg.ratingable_id')
            ->where('rtg.ratingable_type', '=', $this->corrector)
            ->where('c.user_id', '=', $userCriticId)
            ->groupBy('score')
            ->orderBy('score', 'DESC')
            ->get();
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param $arrayForDb
     * @return Response
     */
    public function store( $arrayForDb )
    {
        $result = Rating::create( $arrayForDb );

        if($result){
            $res_arr = array('answer' => 'Ваш голос учтен');
        }else{
            $res_arr = array('answer' => 'Произошла ошибка');
        }
        $res_json = json_encode($res_arr);

        return $res_json;
    }



    /**
     * Update the rating in storage.
     *
     * @param $arrayForDb
     * @param $ratingId
     * @return answer
     */
    public function update($arrayForDb, $ratingId)
    {
        $result = Rating::where('id', $ratingId)
            ->update( $arrayForDb );

        if($result){
            $res_arr = array('answer' => 'Ваш голос учтен');
        }else{
            $res_arr = array('answer' => 'Произошла ошибка');
        }
        $res_json = json_encode($res_arr);

        return $res_json;
    }



    /**
     * Getting rating by entity.
     * Получение рейтинга по сущности.
     *
     * @param $ratingable_type
     * @param $ratingable_id
     * @return Collection
     */
    public function get_by_entity($ratingable_type, $ratingable_id)
    {
        return Rating::where('ratingable_type', '=', $ratingable_type)
            ->where('ratingable_id', '=', $ratingable_id)
            ->select('score AS score')
            ->selectRaw('count(id) AS score_count')
            ->groupBy('score')
            ->orderBy('score', 'DESC')
            ->get();
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rating  $rating
     * @return Response
     */
    public function destroy(Rating $rating)
    {}
}
