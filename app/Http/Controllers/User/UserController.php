<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\CorrectionsController;
use App\Http\Controllers\Elements\ReviewsController;
use App\Http\Controllers\CorrectionController;
use App\Http\Controllers\RatingController;
use Auth;
use App\User;
use App\Element;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public $fields_sort = array(
        'sort-active' => 'По активности',
        'sort-rating' => 'По рейтингу');


    public $ratingable_type = 'user';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $sort = 'sort-active', Request $request)
    {
        if( $q = $request->get('q') ){
            $users = $this->get_search_authors($q);
        }else{
            $users = $this->get_authors($sort);
        }

        if(isset(Auth::user()->id)){
            $user_id = Auth::user()->id;
        }else{
            $user_id = 0;
        }

        if( array_key_exists($sort, $this->fields_sort)){
            $field_sort_selected = $this->fields_sort[$sort];
        }else{
            $field_sort_selected = null;
        }

        return view('users.index',
            [
                'users' => $users,
                'user_auth_id' => $user_id ,
                'field_sort_selected' => $field_sort_selected,
                'q' => $q
            ]

        );
    }



    public function get_authors($sort = 'sort-active', $limit = 50)
    {
        $query = DB::table('users AS u')
            ->leftJoin('ratings', function ($join){
                $join->on( 'ratingable_id', '=', 'u.id')
                    ->where('ratingable_type' , '=', 'user');
            })
            ->select('u.id', 'u.name', 'u.nic', 'u.ban', 'u.ban_time', 'u.created_at')
            ->selectRaw('(SELECT COUNT(es.id) FROM element_strings AS es WHERE es.user_id = u.id ) AS count_element')
            ->selectRaw('SUM(score=3) AS score_3, SUM(score=2) AS score_2, SUM(score=1) AS score_1')
            ->groupBy('u.id');


        if($sort === 'sort-rating'){
            $query->orderBy( 'score_3', 'DESC');
            $query->orderBy( 'score_2', 'DESC');
            $query->orderBy( 'score_1', 'DESC');
        }else {
            $query->orderBy('count_element', 'DESC');
        }

        $users = $query->paginate($limit);

        $users_data = $this->ratingToObject($users);

        return $users_data;
    }


    public function get_search_authors($q, $limit = 10)
    {
        $users = DB::table('users AS u')
            ->leftJoin('ratings', function ($join){
                $join->on( 'ratingable_id', '=', 'u.id')
                    ->where('ratingable_type' , '=', 'user');
            })
            ->select('u.id', 'u.name', 'u.nic', 'u.ban', 'u.ban_time', 'u.created_at')
            ->selectRaw('(SELECT COUNT(es.id) FROM element_strings AS es WHERE es.user_id = u.id ) AS count_element')
            ->selectRaw('SUM(score=3) AS score_3, SUM(score=2) AS score_2, SUM(score=1) AS score_1')
            ->where('u.nic', 'LIKE', "%$q%" )
            ->groupBy('u.id')
            ->get();

        $users_data = $this->ratingToObject($users);

        return $users_data;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {}



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {}



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $user = User::where('users.id', '=', $user_id)
            ->leftJoin('ratings AS r', function($join){
                $userId = '';
                if(isset(Auth::user()->id)){
                    $userId = Auth::user()->id;
                }
                $join->on('users.id', '=', 'r.ratingable_id')
                    ->where('r.ratingable_type', '=', $this->ratingable_type)
                    ->where('r.user_id', '=', $userId );
            })
            ->select('r.score AS rating_score',
                'users.id AS id',
                'users.nic AS nic',
                'users.name AS name',
                'users.middle_name AS middle_name',
                'users.family AS family',
                'users.birth_day AS birth_day'
            )
            ->first();

        $elements = DB::table('element_strings')
            ->leftJoin('elements', 'elements.id', '=', 'element_strings.element_id')
            ->leftJoin('genres', 'elements.genre_id', '=', 'genres.id')
            ->where('element_strings.published', '=', 1)
            ->where('element_strings.user_id', '=', $user_id)
            ->select('elements.id AS e_id', 'elements.message', 'elements.type', 'elements.name AS e_name', 'elements.created_at AS e_created_at', 'elements.updated_at AS e_updated_at', 'elements.parent_id AS e_parent_id',
                'elements.genre_id AS genre_id', 'blocked_user_id', 'elements.blocked AS e_blocked', 'elements.finished AS e_finished', 'elements.slug as e_slug'
                ,'genres.name AS g_name', 'genres.slug AS g_slug'
            )
            ->groupBy('element_strings.element_id')
            ->orderBy('elements.created_at', 'DESC')
            ->get();

        if(isset(Auth::user()->id)){
            $user_auth_id = Auth::user()->id;
        }else{
            $user_auth_id = 0;
        }

        $ReviewsController = new ReviewsController;
        $count_reviews_by_user = $ReviewsController->get_count_reviews_by_user($user_id, 1);

        $CorrectionsController = new CorrectionsController;
        $count_corrections_by_user = $CorrectionsController->get_count_corrections_by_user($user_id, 1);

        // Рейтинг
        $RatingController = new RatingController;
        $ratings = $RatingController->get_by_entity($this->ratingable_type, $user_id);

        // Рейтинг Корректора
        $rating_corrector = $RatingController->get_ratings_by_corrector($user_id);

        // Рейтинг Критика
        $rating_critic = $RatingController->get_ratings_by_critic($user_id);

        return view('users.show', [
            'user' => $user,
            'elements' => $elements,
            'user_auth_id' => $user_auth_id,
            'count_reviews_by_user' => $count_reviews_by_user,
            'count_corrections_by_user' => $count_corrections_by_user,
            'rating_user' => $ratings,
            'rating_critic' => $rating_critic,
            'rating_corrector' => $rating_corrector
        ]);
    }


    public function get_user($user_id)
    {
        return User::where('id', '=', $user_id)->first();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find( $id);
        return view('users.edit', [
            'user' => $user
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =  $request->except('_token', '');
        $user_arr = User::where('id', '=', $id)->update( $data );
        return redirect()->route('a_user.index');
    }

    /**
     * FullText Search
     * Полнотекстовый поиск.
     *
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        if(!$q = $request->get('q')){
            return 'Поиск';
        }
        $count=10;
        $query = mb_strtolower($q, 'UTF-8');
        $arr = explode(" ", $query); //разбивает строку на массив по разделителю
        /*
         * Для каждого элемента массива (или только для одного) добавляет в конце звездочку,
         * что позволяет включить в поиск слова с любым окончанием.
         * Длинные фразы, функция mb_substr() обрезает на 1-3 символа.
         */
        $query = [];
        foreach ($arr as $word)
        {
            $len = mb_strlen($word, 'UTF-8');
            switch (true)
            {
                case ($len <= 3):
                {
                    $query[] = $word . "*";
                    break;
                }
                case ($len > 3 && $len <= 6):
                {
                    $query[] = mb_substr($word, 0, -1, 'UTF-8') . "*";
                    break;
                }
                case ($len > 6 && $len <= 9):
                {
                    $query[] = mb_substr($word, 0, -2, 'UTF-8') . "*";
                    break;
                }
                case ($len > 9):
                {
                    $query[] = mb_substr($word, 0, -3, 'UTF-8') . "*";
                    break;
                }
                default:
                {
                    break;
                }
            }
        }
        $query = array_unique($query, SORT_STRING);
        $qQeury = implode(" ", $query); //объединяет массив в строку
        // Таблица для поиска
        $results = User::whereRaw(
            "MATCH(nic) AGAINST(? IN BOOLEAN MODE)", // nic - поля, по которым нужно искать
            $qQeury)->select('id', 'nic')->paginate($count);

        $arr = array();
        $arr['results'] = array();
        $arr['pagination'] = array('more' => false);
        $i=0;
        foreach ($results as $item){
            if(Auth::user()->id !== $item->id){
                $arr['results'][$i]['id'] = $item->id;
                $arr['results'][$i]['text'] = $item->nic;
                $i++;
            }
        }

        return $arr;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {}
}
