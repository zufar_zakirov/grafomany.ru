<?php

namespace App\Http\Controllers\Admin;

use App\BadWord;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function view;

class BadWordController extends Controller
{
    public function index(Request $request)
    {
        $badwords = BadWord::orderBy('word', 'ASC')->paginate(120);
        return view('admin.badword.index', ['badwords' => $badwords] );
    }

    //
    public function create(Request $request)
    {
        return view('admin.badword.create_word' );
    }

    public function store(Request $request)
    {
        //$words = $request->input('word');
        $words = explode("\r\n", $request->input('word'));
        foreach ($words as $val) {
            if(!empty($val)){
                $badWord = new BadWord;
                $badWord->word = $val;
                $badWord->save();
                unset($badWord);
            }
        }
        return redirect()->route('a_badword.index');
    }

    public function destroy(Request $request)
    {
        DB::table('bad_words')->where('id', '=', $request->input('badword'))->delete();
        return redirect()->route('a_badword.index');
    }

}
