<?php

namespace App\Http\Controllers\Admin;

use App\Genre;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class AdminGenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.genres.genres',
            [
                'genres' => Genre::orderBy('name')->paginate(20)
            ]
        );
    }


    /**
     * Display a listing of the resource.
     */
    public function genres()
    {
        return view('admin.genres.genres',
            [
                'genres' => DB::table('genres')
                    ->leftJoin('elements', 'elements.genre_id', '=', 'genres.id')
                    ->select('genres.id', 'genres.name', 'genres.text', 'genres.title_meta' , 'genres.desc_meta')
                    ->selectRaw('COUNT(elements.id) as count_elements')
                    ->groupBy('genres.id')
                    ->paginate(20)
            ]
        );
    }


    /**
     * Return Genres.
     */
    public function get_genres()
    {}



    /**
     * Return Genre
     * @param Genre $genre
     */
    public function get_genre(Genre $genre)
    {}


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.genres.create_genre' );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $genre = Genre::create($request->all());
        return redirect()->route('a_genre.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Genre $genre
     * @return Response
     */
    public function show(Genre $genre)
    {
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Genre $genre
     * @return Response
     */
    public function edit(Genre $genre)
    {
           return view('admin.genres.edit_genre',[
               'genre' => $genre
           ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Genre $genre
     * @return Response
     */
    public function update(Request $request, Genre $genre)
    {
        $genre->update($request->except('slug'));
        return redirect()->route('a_genre.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Genre $genre
     * @return Response
     * @throws \Exception
     */
    public function destroy(Genre $genre)
    {
        $genre->delete();
        return redirect()->route('a_genre.index');
    }
}
