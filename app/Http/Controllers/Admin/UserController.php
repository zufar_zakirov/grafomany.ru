<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Notification;
use Illuminate\Notifications\Notifiable;
use \App\Notifications\Ban;
use Illuminate\Support\Facades\Notification;

class UserController extends Controller
{
    public $banTimeArr = ['day0'=> 0, 'day7' => 604800, 'day30' => 2592000, 'day60' => 5184000, 'permanently' => 9999994000];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        if ($q = $request->get('q')) {
        }else{
            $q = '';
        }

        $items = User::orderBy('nic')
            ->where('nic', 'LIKE', "%$q%")
            ->paginate(40);
        return view('admin.users.index', [
            'items' => $items,
            'q' => $q
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( User $user )
    {
        return view('admin.users.edit', ['user' => $user ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data =  $request->except('_token', '');
        $user_arr = User::where('id', '=', $id)->update( $data );
        return redirect()->route('user.show', $id);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return redirect()->route('a_user.index');
    }



    public function ban(Request $request)
    {
        $userId = $request->input('userId');
        $banTimeDays = $request->input('banTime');
        $banTime = time() + $this->banTimeArr[$banTimeDays];
        $message = $request->input('message');
        $ban = false;
        if( 'permanently' == $request->input('banTime') ){
            // баним навсегда
            $userUp = User::where('id', '=', $userId)
                ->update(['ban_time' => $banTime, 'ban' => 1]);
            $ban = 1;
        }else{
            // баним на какое-то время
            $userUp = User::where('id', '=', $userId)
                ->update(['ban_time' => $banTime, 'ban' => 0]);
        }

        if( $banTime > time() ){
            $banDate = date('H:i d-m-Y', $banTime);
        }else{
            $banDate = 'Разбанен';
        }

        $user = User::where('id', '=', $userId)->first();
        //$user->notify(new Ban( $banDate,  $message, $ban));
        Notification::send($user, new Ban($banDate,  $message, $ban) );

        return response()->json( ['banDate' => $banDate, 'userId' => $userId] );
    }

}
