<?php

namespace App\Http\Controllers;

use App\Correction;
use App\Element;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\User\UserController;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class CorrectionsController extends Controller
{
    public $perPage = '20';
    public $ratingable_type = 'correction';

    /**
     * Show the form for creating a new resource.
     *
     * @param Element $element
     * @return \Illuminate\Http\Response
     */
    public function create(Element $element)
    {
        $element_string = DB::table('element_strings')
            ->select(DB::raw("GROUP_CONCAT(string SEPARATOR '\n') as string"))
            ->where('element_id', $element->id)
            ->first();

        return view('corrections.create', [
            'correction_text' => $element_string->string,
            'element' => $element
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Element $element
     * @return void
     */
    public function store(Request $request, Element $element)
    {
        $badWordController = new BadWordController();
        $textBadWord = $badWordController->isBadWord($request->input('text'));
        if($textBadWord){
            return json_encode([
                'success' => false,
                'errors' => 'bad-word',
                'message' => 'У вас используется неприемлимое слово',
            ]);
        }

        $correction = Correction::create(
            [
                'text' => $request->input('text'),
                'user_id' => Auth::user()->id,
                'element_id' => $element->id,
                'published' => $request->input('published'),
            ]
        );

        if($request->input('published')){
            $url_redirect = route('correction.show', $correction->id);
        }else{
            $url_redirect = route('correction.edit', $correction->id);
        }

        return json_encode([
            'success' => true,
            'url_redirect' => $url_redirect,
        ]);
    }



    /**
     * Display the specified resource.
     *
     * @param $correction_id
     * @return Response
     */
    public function show( $correction_id )
    {
        $result = DB::table('corrections')
            ->select('corrections.id AS correction_id',
                'corrections.text AS correction_text',
                'corrections.created_at AS correction_created_at',
                'corrections.published AS correction_published',
                'elements.name AS element_name' ,
                'elements.id AS element_id' ,
                'elements.created_at AS element_created_at',
                'genres.name AS genre_name',
                'genres.id AS genre_id',
                'users.id AS user_id',
                'users.name AS user_name',
                'users.nic AS user_nic',
                'r.score AS rating_score'
            )
            ->leftJoin('elements', 'elements.id', '=', 'corrections.element_id')
            ->leftJoin('genres', 'elements.genre_id', '=', 'genres.id')
            ->leftJoin('users', 'users.id', '=', 'corrections.user_id')
            ->leftJoin('ratings AS r', function($join){
                $userId = '';
                if(isset(Auth::user()->id)){
                    $userId = Auth::user()->id;
                }
                $join->on('corrections.id', '=', 'r.ratingable_id')
                    ->where('r.ratingable_type', '=', $this->ratingable_type)
                    ->where('r.user_id', '=', $userId );
            })
            ->where('corrections.id', '=', $correction_id)
            ->first();

        // Рейтинг
        $RatingController = new RatingController;
        $ratings = $RatingController->get_by_entity($this->ratingable_type, $correction_id);

        return view('corrections.show', [
                'correction' => $result,
                'ratings' => $ratings
            ]
        );
    }

    /**
     * Return Corrections by the Element
     *
     * @param $element_id
     * @return Response
     */
    public function show_corrections_by_element($element_id)
    {
        $items = $this->get_corrections_by_element($element_id, $this->perPage);

        return view('corrections.corrections_by_element', [
                'items' => $items
            ]
        );

    }

    /**
     * Return Corrections for the Element
     *
     * @param int $element_id
     * @param int $perPage
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function get_corrections_by_element($element_id , $perPage = 3)
    {
        $result = DB::table('corrections AS c')
            ->leftJoin('elements AS e', function($join){
                $join->on('e.id', '=', 'c.element_id')
                    ->where('c.published', '=', 1);
            })
            ->leftJoin('ratings AS rtg', function ($join){
                $join->on( 'ratingable_id', '=', 'c.id')
                    ->where('ratingable_type' , '=', 'correction');
            })
            ->leftJoin('users AS u', 'u.id', '=', 'c.user_id')
            ->where('e.id', $element_id)
            ->select(
                'c.text as correction_text',
                'c.id as correction_id',
                'c.created_at as correction_created_at',
                'u.id as user_id',
                'u.nic as user_nic',
                'u.name as user_name',
                'e.name AS element_name',
                'e.id AS element_id',
                'e.created_at as element_created_at'
            )
            ->selectRaw('SUM(score=3) AS score_3, SUM(score=2) AS score_2, SUM(score=1) AS score_1')
            ->orderBy('c.created_at', 'DESC')
            ->groupBy('c.id')
            ->paginate($perPage);

        $complite_date = $this->ratingToObject($result);

        return $complite_date;
    }

    public function show_corrections_by_user($user_id)
    {
        $items = $this->get_corrections_by_user( $user_id );
        $userController = new UserController;
        $user = $userController->get_user($user_id);

        return view('corrections.show_by_user', [
                'items' => $items,
                'user' => $user
            ]
        );
    }

    /**
     * @param $user_id
     * @param $element_id
     *
     * @return bool
     */
    public function get_ability_write_for_user($user_id, $element_id)
    {
        $res = Correction::where('user_id', '=', $user_id)
            ->where('element_id', '=', $element_id)
            ->first();
        if($res){$result = false;} else{$result = true;}
        return $result;
    }


    /**
     * Return Corrections for the User
     *
     * @param $user_id
     * @param int $published
     * @return \Illuminate\Support\Collection
     */
    public function get_corrections_by_user($user_id, $published = 1 , $perPage = 30)
    {
        $items = DB::table('corrections AS c')
            ->leftJoin('elements AS e', 'e.id', '=', 'c.element_id')
            ->leftJoin('users AS u', 'u.id', '=', 'c.user_id')
            ->leftJoin('ratings AS rtg', function ($join){
                $join->on( 'ratingable_id', '=', 'c.id')
                    ->where('ratingable_type' , '=', 'correction');
            })
            ->where('c.user_id', $user_id)
            ->where('c.published', '=', $published)
            ->select('e.name AS element_name',
                'e.id AS element_id',
                'e.created_at as element_created_at',
                'c.id AS correction_id',
                'c.text AS correction_text',
                'c.created_at AS correction_created_at',
                'u.id AS user_id',
                'u.nic AS user_nic'
            )
            ->selectRaw('SUM(score=3) AS score_3, SUM(score=2) AS score_2, SUM(score=1) AS score_1')
            ->groupBy('c.id')
            ->paginate($perPage);

        $complite_date = $this->ratingToObject($items);

        return $complite_date;
    }

    /**
     * Return Count Corrections for User
     *
     * @param $user_id
     * @param int $published
     * @return int
     */
    public function get_count_corrections_by_user($user_id, $published = 1)
    {
        $count = DB::table('corrections')
            ->where('corrections.user_id', '=', $user_id)
            ->where('corrections.published', '=', $published)
            ->count('corrections.id');

        return $count;
    }

    /**
     * Return Count Corrections for Element
     *
     * @param $element_id
     * @param int $published
     * @return int
     */
    public function get_count_corrections_by_element( $element_id, $published = 1)
    {
        $count = DB::table('corrections')
            ->where('corrections.element_id', '=', $element_id)
            ->where('corrections.published', '=', $published)
            ->count('corrections.id');
        return $count;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Correction $correction
     * @return void
     */
    public function edit(Correction $correction)
    {
        $item = DB::table('corrections AS c')
            ->select('c.id AS correction_id',
                'c.text AS correction_text',
                'e.id AS element_id' ,
                'e.name AS element_name',
                'e.published AS element_published')
            ->leftJoin('elements AS e', 'e.id', '=', 'c.element_id')
            ->where('c.id', '=', $correction->id)
            ->first();

        return view('corrections.edit',[
            'item' => $item,
            'correction_text' => $item->correction_text
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Correction $correction
     * @return void
     */
    public function update(Request $request, Correction $correction)
    {
        $badWordController = new BadWordController();
        $textBadWord = $badWordController->isBadWord($request->input('text'));
        if($textBadWord){
            return json_encode([
                'success' => false,
                'errors' => 'bad-word',
                'message' => 'У вас используется неприемлимое слово',
            ]);
        }
        $result = $correction->update($request->all());
        if($request->input('published')){
            $url_redirect = route('correction.show', $correction->id);
        }else{
            $url_redirect = route('correction.edit', $correction->id);
        }

        return json_encode([
            'success' => true,
            'url_redirect' => $url_redirect,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {}




    public function corrections_in_work()
    {
        $items = $this->get_corrections_by_user_unpublished( Auth::user()->id );

        return view('corrections.corrections_by_user_unpublished', [
            'items' => $items
        ]);
    }



    /**
     * Return Corrections un published for the user
     *
     * @param $user_id
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function get_corrections_by_user_unpublished($user_id)
    {
        $result = DB::table('users AS u')
            ->select(
                'u.nic AS user_nic',
                'u.id AS user_id',
                'c.id AS correction_id',
                'c.created_at AS correction_created_at',
                'c.text AS correction_text',
                'e.id AS element_id',
                'e.name AS element_name'
            )
            ->leftJoin('corrections AS c', 'c.user_id', '=', 'u.id')
            ->leftJoin('elements AS e', 'e.id', '=', 'c.element_id')
            ->where('u.id', '=', $user_id)
            ->where('c.published', '=', 0)
            ->paginate(20);
        return $result;
    }


}
