<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $fields_sort = array(
        'sort-date-asc' => 'Сначала старые',
        'sort-date-desc' => 'Сначала новые',
        'sort-rating-desc' => 'Сначала популярные' );


    public function ratingToObject($arr)
    {
        foreach ($arr->all() as $key => $value){
            $value->ratings = collect([
                (object)['score_count' => $value->score_3, 'score' => 3 ],
                (object)['score_count' => $value->score_2, 'score' => 2 ],
                (object)['score_count' => $value->score_1, 'score' => 1 ]
            ]);
        }

        return $arr;
    }

    /**
     *  Возвращает нужную словоформу.
     * @param
     * @param string $form1
     * @param string $form2
     * @param string $form3
     * @return mixed|string
     */
    public function tplWordForm($n, $first_form = false, $form1 = 'год', $form2='года', $form3='лет')
    {

        if($first_form==='минута'){
            $form1='минута';
            $form2='минуты';
            $form3='минут';
        }elseif ($first_form==='секунда'){
            $form1 = 'секунда';
            $form2='секунды';
            $form3='секунд';
        }elseif ($first_form==='час'){
            $form1 = 'час';
            $form2='часа';
            $form3='часов';
        }elseif ($first_form === 'день'){
            $form1='день';
            $form2='дня';
            $form3='дней';
        }


        $n = abs($n) % 100;
        $n1 = $n % 10;

        if ($n > 10 && $n < 20) {
            return $form3;
        }

        if ($n1 > 1 && $n1 < 5) {
            return $form2;
        }

        if ($n1 == 1) {
            return $form1;
        }

        return $form3;
    }


    /**
     *  Возвращает разницу во времени. В днях, часах или минитух и словоформу
     * @param $date_time
     * @param $addDays
     * @return array
     */
    public function diffTimeFromNow($date_time, $addDays)
    {
        $date_end = Carbon::createFromDate($date_time);
        $date = $date_end->addDays($addDays);
        $now = Carbon::now();

        if( $remain = $now->diffInDays( $date ) ){
            $word = $this->tplWordForm($remain, 'день');

        }elseif( $remain = $now->diffInHours( $date ) ){
            $word = $this->tplWordForm($remain, 'час');

        }elseif( $remain = $now->diffInMinutes( $date ) ){
            $word = $this->tplWordForm($remain, 'минута');

        }elseif( $remain = $now->diffInSeconds( $date ) ){
            $word = $this->tplWordForm($remain, 'секунда');

        }

        $obj = new \stdClass();
        $obj->number = $remain;
        $obj->word = $word;
        return $obj;
        //return ['number' => $remain, 'word' => $word];
    }

}
