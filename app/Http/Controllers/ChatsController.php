<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use App\Message;
use App\Chat;
use App\MessageStatus;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\BadWordController;

class ChatsController extends Controller
{
    /**
     * Получаем список чатов для авторизованного пользователя
     *
     * @return object
     */
    public function getOnUser()
    {
        $user_id = auth()->id();
        $chat_user =  DB::table('user_chat AS uch')
            ->leftJoin('users AS u', 'u.id', '=', 'uch.user_id')
            ->leftJoin(DB::raw('(Select MAX(created_at) AS message_created_at,
                chat_id AS message_chat_id, text AS message_text
                FROM messages GROUP BY chat_id ) AS conv'), function($join){
                $join->on('uch.chat_id', '=', 'conv.message_chat_id');
            })
            ->leftJoin('message_status AS ms',  function($join) use ($user_id){
                $join->on('ms.chat_id', '=', 'uch.chat_id' )->where('ms.user_id', '=', $user_id)->where('is_read', '=', 0);
            })
            ->select('uch.user_id AS user_id', 'u.nic AS user_nic', 'uch.chat_id AS chat_id',
                'message_created_at', 'ms.is_read AS message_read')
            ->whereIn('uch.chat_id', function($query) use($user_id){
                $query->select('chat_id')->from('user_chat')->where('user_id', '=', $user_id)->where('leave_chat', '=', 0)->get();
            })
            ->orderBy('message_created_at', 'DESC')
            ->get();

        $arr_chats = [];
        foreach ($chat_user as $key => $val_arr){
            $arr_chats[] = $val_arr->chat_id;
        }

        $return_arr = [];
        $i = 1;
        foreach ($chat_user as $u_ch){
            foreach ($arr_chats as $c){
                if( $c == $u_ch->chat_id ){
                    $return_arr[$c]['chat_id'] = $u_ch->chat_id;
                    $return_arr[$c]['datetime'] = $u_ch->message_created_at;
                    $return_arr[$c]['message_read'] = $u_ch->message_read;
                    if($u_ch->user_id != auth()->id()){
                        $return_arr[$c]['user'][$u_ch->user_id]['id'] = $u_ch->user_id;
                        $return_arr[$c]['user'][$u_ch->user_id]['nic'] = $u_ch->user_nic;
                    }

                }
            }
            $i++;
        }

        $ret = [];
        $i = 1;
        foreach ($return_arr as $chat){
            $ret[$i] = $chat;
            $i++;
        }

        return $ret;
    }


    /**
     * Создаем чат
     *
     * @return array
     */
    public function create(Request $request)
    {
        $message_text = $request->input('message');

        $chat = Chat::create(['user_id' => Auth::user()->id ]);
        foreach ($request->input('value') as $user){
            DB::table('user_chat')->insert([
                'chat_id' => $chat->id, 'user_id' => $user['id'],
            ]);
        }
        DB::table('user_chat')->insert([
            'chat_id' => $chat->id, 'user_id' => Auth::user()->id,
        ]);

        $message = new Message;
        $message->user_id = Auth::user()->id;
        $message->chat_id = $chat->id;
        $message->text = $message_text;
        $message->save();

        foreach ($request->input('value') as $user){
            if($user['id'] == auth()->id()){
                continue;
            }
            $messageStatus = new MessageStatus;
            $messageStatus->user_id = $user['id'];
            $messageStatus->is_read = 0;
            $messageStatus->message_id = $message->id;
            $messageStatus->chat_id = $chat->id;
            $messageStatus->save();
            unset($messageStatus);
        }

        return $chat->id;
    }


    /**
     * Получаем сообщения по ID чата
     *
     * @return array
     */
    public function getMessagesByChat($chat_id)
    {
        $this->setReadMessagesByChat($chat_id);

        $messages = DB::table('messages')
            ->leftJoin('users AS u', 'u.id', '=', 'messages.user_id')
            ->select('u.nic AS user_nic', 'u.id AS user_id', 'messages.chat_id AS chat_id', 'messages.text AS text', 'messages.created_at AS created_at')
            ->orderBy('messages.created_at', 'ASC')
            ->where('chat_id', '=', $chat_id)
            ->get();

        foreach ($messages as $k => $v){
            $kCh = 'm_'. $k;
            $messages[$k]->myId = Auth::user()->id;
        }
        return response()->json($messages);
    }

    /**
     * Помечаем все сообщения прочитанными в чате
     * @param $chat_id int
     */
    public function readMessagesByChat(Request $request)
    {
        $chat_id = $request->chat_id;
        $this->setReadMessagesByChat($chat_id);
        return response()->json($chat_id);
    }

    /**
     * Помечаем все сообщения прочитанными в чате
     * @param $chat_id int
     */
    public function setReadMessagesByChat($chat_id)
    {
        MessageStatus::where('chat_id', '=', $chat_id)->update(['is_read' => 1]) ;
    }


    /**
     * Отправляем сообщение в чат.
     * @param Request $request
     *
     * @return json
     */
    public function send(Request $request)
    {
        $badWordController = new BadWordController();
        $textBadWord = $badWordController->isBadWord($request->input('text'));
        if($textBadWord){
            return json_encode([
                'success' => false,
                'errors' => 'bad-word',
                'message' => 'У вас используется неприемлимое слово',
            ]);
        }

        $message = Message::create([
            'user_id' => auth()->id(),
            'text' => $request->text,
            'chat_id' => $request->chat_id,
        ]);

        $user = User::where('id', '=', auth()->id())->first();
        $message->user_nic = $user['nic'];
        $message->myId = auth()->id();
        broadcast(new NewMessage($message));

        $usersFromChat = $this->getUsersListByChat($request->chat_id);
        foreach ($usersFromChat as $u){
            if($u->user_id == auth()->id()){
                continue;
            }

            $messageStatus = new MessageStatus;
            $messageStatus->user_id = $u->user_id;
            $messageStatus->is_read = 0;
            $messageStatus->message_id = $message->id;
            $messageStatus->chat_id = $request->chat_id;
            $messageStatus->save();
            unset($messageStatus);
        }

        return response()->json($message);
    }


    /**
     * Получаем users из чата
     * @param integer chat_id
     *
     * @return array
     */
    public function getUsersListByChat($chat_id)
    {
        $users = DB::table('user_chat')->where('chat_id','=', $chat_id)->where('leave_chat', '', 0)->get();
        return $users;
    }


    /**
     *  Выйти из чата
     *
     * @params $chat_id
     * return result
     */
    public function leave_chat(Request $request)
    {
        $chat_id = $request->chat_id;
        $res = DB::table('user_chat')->where('user_id', '=', auth()->id())->where('chat_id', '=', $chat_id)->update(['leave_chat' => 1]);
        return $res;
    }


    /**
     * Получаем кол-во не прочитанных сообщений для авторизованного пользователя
     *
     * @return int
     */
    public function getCountUnreadMessagesByUser() : int
    {
        //$return = Chat::where('user_id', '=', auth()->id())->where('is_read', '=', 0)->count();
        //$return = Chat::where('user_id', '=', auth()->id())->where('is_read', '=', 0)->count();
        $return = DB::table('message_status')
            ->where('user_id', '=', auth()->id())
            ->where('is_read', '=', 0)
            ->groupBy('chat_id')
            ->count();

        return $return;
    }

}
