<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function list()
    {
        // Получение объекта пользователя
        $user = User::where('id', '=', Auth::user()->id)->first();

        // Получение всех уведомлений
        $notices = $user->notifications;

        // добавляем аттрибуты: прочитано или нет. для view
        foreach ($notices as $key => $notice){
            if($notice->read_at){
                $notices[$key]->read = 'read';
            }else{
                $notices[$key]->read = 'unread';
            }
        }

        // Помечаем все прочитаными
        $this->readAll();

        return view('notifications.list_by_user', [
                'notices' => $notices
            ]
        );
    }


    public function readAll()
    {
        Auth::user()->notifications->markAsRead();
    }



    public function getNoticeCount()
    {
        return count(Auth::user()->unreadNotifications);
    }



    public function remove(Request $request)
    {
        $notice = Auth::user()->notifications()
            ->where('id', '=', $request->noticeId)
            ->delete();

        return redirect()->route('notifications');
    }



}
