<?php

namespace App\Http\Controllers\Elements;

use App\Element;
use Auth;
use App\ElementVoteRemove;
use App\ElementString;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ElementVoteRemoveController extends Controller
{

    /**
     * Set Vote
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function vote_remove_set(Request $request)
    {
        $res = $this->isRemoveOrPublic( $request->get('element_id') );


        if($request->get('vote_remove') || $request->get('element_id')){
            $vote = new ElementVoteRemove;
            $vote->answer = $request->get('vote_remove');
            $vote->element_id = $request->get('element_id');
            $vote->user_id = Auth::user()->id;
            $vote->save();
        }
        return redirect()->route('element.show', $request->input('element_id') );
    }


    /**
     * Get Vote
     *
     * @param $element_id
     * @param $user_id
     * @return RedirectResponse
     */
    public function get_vote($element_id, $user_id)
    {
        return ElementVoteRemove::where('element_id', '=', $element_id)
            ->where('user_id','=', $user_id)
            ->first();
    }


    /**
     * Метод скрывает Element, если кол-во голосов равно кол-ву соавторов и все проголосовали за удаление.
     * The method hides Element, if the number of votes is equal to the number of co-authors and everyone voted to remove it.
     *
     * @param $element_id
     * @return mixed
     */
    public function isRemoveOrPublic($element_id)
    {
        // Кол-во голосов
        $votes =  ElementVoteRemove::where('element_id', '=', $element_id)
            ->get();

        // Кол-во авторов в элементах
        $countUsers = ElementString::where('element_id', '=', $element_id)->groupBy('user_id')
            ->count();

        $hide_element = false;
        // Если кол-во голосов равно кол-ву авторов, то проверяем все голоса. Есть ли хоть один за публикацию.
        if( (int)count( $votes ) === (int)$countUsers){
            $hide_element = true;
            foreach ($votes as $vote ){
                if( (int)$vote->answer === 1 ){
                    $hide_element = false;
                }
            }
        }

        if($hide_element){
            $element = Element::find($element_id);
            $element->published = 0;
            $element->finished = 1;
            $element->save();
        }
        return $votes;
    }





}
