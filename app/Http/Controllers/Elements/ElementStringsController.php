<?php

namespace App\Http\Controllers\Elements;

use App\Http\Controllers\BadWordController;
use App\Http\Controllers\GenresController;
use Auth;
use App\Element;
use App\ElementString;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ElementStringsController extends Controller
{

    public $genry_max_stroke__haiku = 3;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }



    /**
     * Show the form for creating a new resource.
     *
     * @param $element_id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function create( $element_id )
    {
        $element_string = DB::table('element_strings')
            ->leftJoin('users', 'users.id', '=', 'element_strings.user_id')
            ->where('element_id', $element_id)
            ->get();

        $element_arr = DB::table('elements')
            ->leftJoin('genres', 'genre_id', '=', 'genres.id')
            ->leftJoin('element_strings', 'element_strings.id', '=', 'elements.id')
            ->leftJoin('users', 'users.id', '=', 'element_strings.user_id')
            ->where('elements.id', $element_id)
            ->select('elements.id as e_id', 'elements.message', 'elements.type', 'elements.name as e_name', 'elements.created_at as e_created_at', 'elements.updated_at as e_updated_at', 'elements.parent_id as e_parent_id',
                'elements.genre_id as genre_id', 'blocked_user_id', 'elements.blocked as e_blocked', 'elements.finished as e_finished', 'elements.slug as e_slug',

                'users.id as user_id', 'users.name as user_name', 'users.name as user_name',

                'genres.name as g_name', 'genres.slug as g_slug', 'genres.letters_for_string'
            )
            ->first();

        $this->block_element($element_id);


        return view('elements.create_string', [
            'element' => $element_arr,
            'element_strings' => $element_string
        ]);
    }


    /**
     * Block the Element for up to 5 minutes.
     *
     * @param  $element_id
     * @return \Illuminate\Http\Elements\Element
     * @throws \Exception
     */
    public function block_element($element_id)
    {
        if( !Auth::user()->id ){
            return false;
        }

        $time_carbon = new Carbon('now');
        $time_carbon->addMinute(5);

        $element = Element::find($element_id);
        $element->blocked_user_id = Auth::user()->id ;
        $element->blocked_time = $time_carbon ;
        $element->save();
        return $element;
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = false;



        $badWordController = new BadWordController();
        $esBadWord = $badWordController->isBadWord($request->input('string'));
        if($esBadWord){
            return json_encode([
                'success' => false,
                'errors' => 'bad-word',
                'message' => 'У вас используется неприемлимое слово',
            ]);
        }

        $user_id = Auth::user()->id;
        $element_id = $request->input('element_id');
        // может ли user писать
        $res_can_write = $this->can_write_string_user($user_id ,$element_id);

        $finished = 0;
        $element = Element::where('id', '=', $element_id)->first();
        $element_string_count = (int)ElementString::where('element_id', '=', $element_id)->count();

        $genresController = new GenresController;
        $number_lines_genre = $genresController->get_number_lines_by_id($element->genre_id);

        if((int)$number_lines_genre->number_lines !== 0 && $element_string_count >= (int)$number_lines_genre->number_lines-1 ){
            $finished = 1;
        }

        if( $res_can_write ){
            $element_string = ElementString::create([
                'string' => $request->input('string'),
                'user_id' => $user_id,
                'element_id' => $element_id,
            ]);
            Element::where('id', $request->input('element_id') )
                ->update( ['blocked_time' => null, 'finished' => $finished ]);
        }

        //return redirect()->route('element.show', $request->input('element_id') );
        $url_redirect = route( 'element.show',  $element_id);
        return json_encode([
            'success' => true,
            'url_redirect' => $url_redirect,
        ]);
    }

    /**
     * Проверяем может ли User написать строку в элемент.
     *
     * @param $user_id
     * @param $element_id
     *
     * @return bool
     */
    public function can_write_string_user($user_id, $element_id)
    {
        if( !isset( Auth::user()->id ) ){
            return false;
        }

        $possibility_write_for_user = DB::table('element_strings AS es')
            ->select('es.id AS es_id',
                'es.created_at AS created_at',
                'es.string AS string',
                'u.nic AS user_nic',
                'u.name AS user_name',
                'u.id AS user_id'
            )
            ->leftJoin('users AS u', 'u.id', '=', 'es.user_id')

            ->where('element_id', $element_id )
            ->where('user_id', Auth::user()->id )
            ->where('es.created_at', '>', Carbon::now()->subHours(24) )
            ->first();


        if(!$possibility_write_for_user){
            $res = true;
        }else{
            $res = false;
        }

        return $res;
    }



    /**
     * @param $user_id
     * @param $element_id
     *
     * @return bool
     */
    public function check_write_to_element_for_user($user_id, $element_id)
    {
        $res = ElementString::where('user_id', '=', $user_id)
            ->where('element_id', '=', $element_id)
            ->first();
        if($res){$result = false;} else{$result = true;}
        return $result;
    }



}
