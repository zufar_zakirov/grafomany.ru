<?php

namespace App\Http\Controllers\Elements;

use App\Correction;
use App\Element;
use App\ElementString;
use App\Genre;
use App\Http\Controllers\BadWordController;
use App\Http\Controllers\BookmarksController;
use App\Http\Controllers\CorrectionsController;
use App\Http\Controllers\GenresController;
use App\Http\Controllers\RatingController;
use Auth;
use App\User;
use App\Http\Controllers\Elements\ElementVoteRemoveController;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
//use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;

class ElementsController extends Controller
{


    public $ratingable_type = 'element';
    public $genry_max_stroke__haiku = 3;

    //
    public function elements(Request $request)
    {
        if ($q = $request->get('q')) {
            $elements = $this->get_search_elements($q);
        } else {
            $elements = $this->get_elements($request->get('g_id'));
        }



        $genres = DB::table('genres')->get();
        $genre = DB::table('genres')->where('id', '=', $request->get('g_id'))->first();

        return view('elements.elements',
                [
                    'elements' => $elements,
                    'genres' => $genres,
                    'genre_selected' => $genre,
                    'q' => $q
                ]
        );
    }


    public function get_elements($genre_id, $perPage = 20)
    {
        $operator = '<>';
        if ($genre_id) {
            $operator = '=';
        }

        $elements = DB::table('elements')
                ->leftJoin('genres', 'genre_id', '=', 'genres.id')
                ->select('elements.id AS e_id', 'elements.message', 'elements.type', 'elements.name AS e_name',
                                    'elements.created_at AS e_created_at', 'elements.updated_at AS e_updated_at', 'elements.parent_id AS e_parent_id',
                        'elements.genre_id AS genre_id', 'blocked_user_id', 'elements.blocked AS e_blocked', 'elements.finished AS e_finished', 'elements.slug AS e_slug', 'elements.blocked_time'
                        ,'genres.name as g_name', 'genres.slug as g_slug', 'genres.id AS genre_id'
                )
                ->where('elements.finished', '=', 0)
                ->where('elements.created_at', '>', Carbon::now()->subDays(14))
                ->where('elements.genre_id', $operator, $genre_id)
                ->where('elements.group', '=', 0)
                ->where('elements.published', '=', 1)
                ->groupBy('elements.id')
                ->orderBy('elements.created_at', 'DESC')
                ->paginate($perPage);

        // добавление в объект периода времени остатка до завершения
        foreach ($elements as $key => $element) {
            $res = $this->diffTimeFromNow($element->e_created_at, 14);
            $element->remains = $res;
        }


        return $elements;
    }



    public function elements_group(Request $request)
    {
        if ($q = $request->get('q')) {
            $elements = $this->get_search_elements_group($q);
        } else {
            $elements = $this->get_elements_group($request->get('g_id'));
        }

        // добавление в объект периода времени остатка до завершения
        foreach ($elements as $key => $element) {
            $res = $this->diffTimeFromNow($element->e_created_at, 14);
            $element->remains = $res;
        }
        $genres = DB::table('genres')->get();
        $genre = DB::table('genres')->where('id', '=', $request->get('g_id'))->first();

        return view('elements.elements_group',
                [
                    'elements' => $elements,
                    'genres' => $genres,
                    'genre_selected' => $genre,
                    'q' => $q
                ]
        );
    }



    public function get_elements_group($genre_id = null, $perPage = 20)
    {
        $operator = '<>';
        if ($genre_id) {
            $operator = '=';
        }

        $elements = DB::table('element_group')
                ->leftJoin('elements', 'element_group.element_id', '=', 'elements.id')
                ->leftJoin('genres', 'elements.genre_id', '=', 'genres.id')
                ->select('elements.group as e_group', 'elements.id as e_id', 'elements.message', 'elements.type', 'elements.name as e_name',
                'elements.created_at as e_created_at', 'elements.updated_at as e_updated_at', 'elements.parent_id as e_parent_id',
                'elements.genre_id as genre_id', 'blocked_user_id', 'elements.blocked as e_blocked', 'elements.finished as e_finished',
                'elements.slug as e_slug', 'elements.blocked_time',
                'genres.name as g_name', 'genres.slug as g_slug', 'genres.id AS genre_id'
                )
                ->where('elements.finished', '=', 0)
                ->where('elements.created_at', '>', Carbon::now()->subDays(14))
                ->where('elements.genre_id', $operator, $genre_id)
                ->where('elements.group', '=', 1)
                ->where('element_group.user_id', '=', Auth::user()->id)
                ->where('elements.published', '=', 1)
                ->groupBy('elements.id')
                ->orderBy('elements.created_at', 'DESC')
                ->paginate($perPage);
        return $elements;
    }



    public function get_search_elements($q)
    {
        $elements = DB::table('elements')
                ->leftJoin('genres', 'genre_id', '=', 'genres.id')
                ->leftJoin('element_strings', 'element_strings.element_id', '=', 'elements.id')
                ->leftJoin('users', 'users.id', '=', 'element_strings.user_id')
                ->select('elements.id as e_id', 'elements.message', 'elements.type', 'elements.name as e_name', 'elements.created_at as e_created_at', 'elements.updated_at as e_updated_at', 'elements.parent_id as e_parent_id',
                        'elements.genre_id as genre_id', 'blocked_user_id', 'elements.blocked as e_blocked', 'elements.finished as e_finished', 'elements.slug as e_slug', 'elements.blocked_time',
                        'element_strings.string as es_string', 'element_strings.created_at as es.created_at',
                        'genres.name as g_name', 'genres.slug as g_slug', 'genres.id AS genre_id',
                        'nic AS u_nic', 'users.name as u_name', 'users.id AS u_id'
                )
                ->where('elements.finished', '=', 0)
                ->where('elements.created_at', '>', Carbon::now()->subDays(14))
                ->where('elements.name', 'LIKE', "%$q%")
                ->where('elements.published', '=', 1)
                ->groupBy('element_strings.element_id')
                ->orderBy('elements.created_at', 'DESC')
                ->paginate();
        return $elements;
    }



    public function get_search_elements_group($q)
    {
        $elements = DB::table('element_group')
                ->leftJoin('elements', 'element_group.element_id', '=', 'elements.id')
                ->leftJoin('genres', 'genre_id', '=', 'genres.id')
                ->leftJoin('element_strings', 'element_strings.element_id', '=', 'elements.id')
                ->leftJoin('users', 'users.id', '=', 'element_strings.user_id')
                ->select('elements.id as e_id', 'elements.message', 'elements.type', 'elements.name as e_name', 'elements.created_at as e_created_at', 'elements.updated_at as e_updated_at', 'elements.parent_id as e_parent_id',
                        'elements.genre_id as genre_id', 'blocked_user_id', 'elements.blocked as e_blocked', 'elements.finished as e_finished', 'elements.slug as e_slug', 'elements.blocked_time',
                        'element_strings.string as es_string', 'element_strings.created_at as es.created_at',
                        'genres.name as g_name', 'genres.slug as g_slug', 'genres.id AS genre_id',
                        'nic AS u_nic', 'users.name as u_name', 'users.id AS u_id'
                )
                ->where('elements.finished', '=', 0)
                ->where('elements.created_at', '>', Carbon::now()->subDays(14))
                ->where('elements.name', 'LIKE', "%$q%")
                ->where('elements.group', '=', 1)
                ->where('element_group.user_id', '=', Auth::user()->id)
                ->where('elements.published', '=', 1)
                ->groupBy('element_strings.element_id')
                ->orderBy('elements.created_at', 'DESC')
                ->paginate();
        return $elements;
    }



    /**
     * Method for View Finished Elements. With Search and Range and Filter
     * Метод вывод оконченных произведений с поиском и сортировкой и фильтром
     * @param string $sort
     * @param Request $request
     * @return Factory|View
     */
    public function elements_finished($sort = 'sort-date-desc', Request $request)
    {
        $genre = false;

        if ($request->get('sort')) {
            $sort = $request->get('sort');
        }

        if ($q = $request->get('q')) {
            $elements = $this->get_search_elements_finished($q, $sort);
        } elseif ($genre_id = $request->get('g_id')) {
            $elements = $this->get_elements_finished('20', $genre_id, $sort);
            $genre = Genre::where('id', '=', $genre_id)->first();
        } else {
            $elements = $this->get_elements_finished(20, null, $sort);
        }

        $genres = DB::table('genres')->get();

        if (array_key_exists($sort, $this->fields_sort)) {
            $field_sort_selected = $this->fields_sort[$sort];
        } else {
            $field_sort_selected = null;
        }

        return view('elements.elements_finished',
                [
                    'elements' => $elements,
                    'genres' => $genres,
                    'genre_selected' => $genre,
                    'field_sort_selected' => $field_sort_selected,
                    'sort' => $sort,
                    'q' => $q
                ]
        );
    }


    public function elements_finished_by_genre(Request $request, Genre $genre = null, $sort = 'sort-date-desc')
    {
        if (array_key_exists($sort, $this->fields_sort)) {
            $field_sort_selected = $this->fields_sort[$sort];
        } else {
            $field_sort_selected = null;
        }

        return $elements = $this->get_elements_finished(20, $genre->id, $sort);
    }


/*
    public function get_elements_finished($perPage = 20, $genre_id = null, $sort = 'sort-date-desc')
    {
        if ($genre_id) {
            $operator = '=';
        } else {
            $operator = '<>';
            $genre_id = 0;
        }

        $query = DB::table('elements AS e')
                ->leftJoin('genres', 'genre_id', '=', 'genres.id')
                ->leftJoin('users', 'users.id', '=',
                        DB::raw("(SELECT user_id FROM element_strings AS e_s WHERE e_s.element_id = e.id ORDER BY e_s.id LIMIT 0,1)")
                )
                ->leftJoin('ratings', function ($join) {
                    $join->on('ratingable_id', '=', 'e.id')
                    ->where('ratingable_type', '=', 'element');
                })
                ->select('e.id AS e_id', 'e.message AS e_message',
                    'e.name AS e_name', 'e.created_at AS e_created_at', 'e.updated_at AS e_updated_at',
                    'e.genre_id AS genre_id', 'blocked_user_id', 'e.blocked AS e_blocked',
                    'e.finished AS e_finished', 'e.slug AS e_slug', 'e.blocked_time',

                    'genres.name AS g_name', 'genres.slug AS g_slug', 'genres.id AS genre_id'
//                    ,'nic AS u_nic', 'users.name AS u_name', 'users.id AS u_id'
                )
                ->selectRaw('SUM(score=3) AS score_3, SUM(score=2) AS score_2, SUM(score=1) AS score_1')
                ->selectRaw('(SELECT COUNT(es.id) FROM element_strings AS es WHERE es.element_id =e.id ) AS count_element_strings')
                ->where(function ($query) {
                    $query->where('e.created_at', '<', Carbon::now()->subDays(14))
                    ->orWhere('e.finished', '=', 1);
                })
                ->where('e.genre_id', $operator, $genre_id)
                ->where('e.published', '=', 1)
                ->groupBy('e.id');

        if ($sort === 'sort-date-asc') {
            $query->orderBy('e.created_at', 'ASC');
        } elseif ($sort === 'sort-date-desc') {
            $query->orderBy('e.created_at', 'DESC');
        } elseif ($sort === 'sort-rating-desc') {
            $query->orderBy('score_3', 'DESC');
            $query->orderBy('score_2', 'DESC');
            $query->orderBy('score_1', 'DESC');
        }

        $elements = $query->paginate($perPage);

        $elements_data = $this->ratingToObject($elements);

        return $elements_data;
    }
*/


    public function get_elements_finished($perPage = 20, $genre_id = null, $sort = 'sort-date-desc')
    {
        if ($genre_id) {
            $operator = '=';
        } else {
            $operator = '<>';
            $genre_id = 0;
        }

        $query = DB::table('elements AS e')
            ->leftJoin('genres', 'genre_id', '=', 'genres.id')
            ->leftJoin('users', 'users.id', '=',
                DB::raw("(SELECT user_id FROM element_strings AS e_s WHERE e_s.element_id = e.id ORDER BY e_s.id LIMIT 0,1)")
            )
            ->leftJoin('ratings', function ($join) {
                $join->on('ratingable_id', '=', 'e.id')
                    ->where('ratingable_type', '=', 'element');
            })
            ->select('e.id AS e_id', 'e.message', 'e.type', 'e.name AS e_name', 'e.created_at AS e_created_at', 'e.updated_at AS e_updated_at', 'e.parent_id AS e_parent_id',
                'e.genre_id AS genre_id', 'blocked_user_id', 'e.blocked AS e_blocked', 'e.finished AS e_finished', 'e.slug AS e_slug', 'e.blocked_time',
                'genres.name AS g_name', 'genres.slug AS g_slug', 'genres.id AS genre_id'
//                    ,'nic AS u_nic', 'users.name AS u_name', 'users.id AS u_id'
            )
            ->selectRaw('SUM(score=3) AS score_3, SUM(score=2) AS score_2, SUM(score=1) AS score_1')
            ->selectRaw('(SELECT COUNT(es.id) FROM element_strings AS es WHERE es.element_id =e.id ) AS count_element_strings')
            ->where(function ($query) {
                $query->where('e.created_at', '<', Carbon::now()->subDays(14))
                    ->orWhere('e.finished', '=', 1, 'or');
            })
            ->where('e.genre_id', $operator, $genre_id)
            ->where('e.published', '=', 1)
            ->groupBy('e.id');

        if ($sort === 'sort-date-asc') {
            $query->orderBy('e.created_at', 'ASC');
        } elseif ($sort === 'sort-date-desc') {
            $query->orderBy('e.created_at', 'DESC');
        } elseif ($sort === 'sort-rating-desc') {
            $query->orderBy('score_3', 'DESC');
            $query->orderBy('score_2', 'DESC');
            $query->orderBy('score_1', 'DESC');
        }

        $elements = $query->paginate($perPage);

        $elements_data = $this->ratingToObject($elements);

        return $elements_data;
    }





    public function get_search_elements_finished($q, $sort)
    {
        $query = DB::table('elements AS e')
                ->leftJoin('genres', 'genre_id', '=', 'genres.id')
                ->leftJoin('ratings', function ($join) {
                    $join->on('ratingable_id', '=', 'e.id')
                    ->where('ratingable_type', '=', 'element');
                })
                ->select('e.id AS e_id', 'e.message', 'e.type',
                    'e.name AS e_name', 'e.created_at AS e_created_at', 'e.updated_at AS e_updated_at',
                    'e.parent_id AS e_parent_id',
                    'e.genre_id AS genre_id', 'blocked_user_id', 'e.blocked AS e_blocked',
                    'e.finished AS e_finished', 'e.slug AS e_slug', 'e.blocked_time',
                    'genres.name AS g_name', 'genres.slug AS g_slug', 'genres.id AS genre_id'
                )
                ->selectRaw('SUM(score=3) AS score_3, SUM(score=2) AS score_2, SUM(score=1) AS score_1')
                ->selectRaw('(SELECT COUNT(es.id) FROM element_strings AS es WHERE es.element_id =e.id ) AS count_element_strings')
                ->where(function ($query) {
                    $query->where('e.created_at', '<', Carbon::now()->subDays(14))
                    ->orWhere('e.finished', '=', 1, 'or');
                })
                ->where('e.name', 'LIKE', "%$q%")
                ->where('e.published', '=', 1)
                ->groupBy('e.id');

        if ($sort === 'sort-date-asc') {
            $query->orderBy('e.created_at', 'ASC');
        } elseif ($sort === 'sort-date-desc') {
            $query->orderBy('e.created_at', 'DESC');
        } elseif ($sort === 'sort-rating-desc') {
            $query->orderBy('score_3', 'DESC');
            $query->orderBy('score_2', 'DESC');
            $query->orderBy('score_1', 'DESC');
        }

        $elements = $query->paginate();

        $elements_data = $this->ratingToObject($elements);

        return $elements_data;
    }



    public function create()
    {
        return view('elements.create_element', [
            'elements' => [],
            'genres' => Genre::get()
        ]);
    }


    /**
     * @param Element $element
     * @return Factory|View
    */
    public function show(Element $element, Request $request)
    {
        \Cache::put('fsdft', 'textaom' , now()->addMinutes(30) );
        $isPublished = $this->isPublished( $element->id );
        $show = true;
        if(!$isPublished){
            if( isset(Auth::user()->id) ){
                $isCoAuthor = $this->isCoAuthor( Auth::user()->id, $element->id );
                if( !$isCoAuthor ){
                    $show = false;
                }else{
                    return $this->element($element, 'elements.element_draft');
                }
            }else{
                $show = false;
            }
        }

        // произведение закрыто для публикации
        if(!$show){
            return view('elements.no_show');
        }

        if( $this->isFinishedElement($element->id) ){
            return $this->element_finished($element, $request);
        }else{
            return $this->element($element);
        }
    }


    /**
     * Возвращает шаблон с элементом, который в работе
     * Return view with element, which in work
     * @param Element $element
     * @param string $view
     * @return Factory|View
     */
    public function element(Element $element, $view = 'elements.show')
    {
        $array = $this->get_element($element->id);
        $array['element_arr']->remains = $this->diffTimeFromNow($array['element_arr']->e_created_at, '14');
        $users_group = $this->get_users_group_by_element($element->id);

        // Записываем в переменную 1 или 0. 0 - автор писал строку менее 24 часов назад.
        $possibility_write_for_user = 0;
        if (isset(Auth::user()->id)) {
            // проверяем может ли автор писать в это произведение
            $possibility_write_for_user = DB::table('element_strings AS es')
                ->select('es.id AS es_id',
                    'es.created_at AS created_at',
                    'es.string AS string',
                    'u.nic AS user_nic',
                    'u.name AS user_name',
                    'u.id AS user_id'
                )
                ->leftJoin('users AS u', 'u.id', '=', 'es.user_id')
                ->where('element_id', $element->id)
                ->where('user_id', Auth::user()->id)
                ->where('es.created_at', '>', Carbon::now()->subHours(24))
                ->first();

            if ($possibility_write_for_user) {
                $now = Carbon::now();
                $carbon_time_string = Carbon::create($possibility_write_for_user->created_at);
                $add_time_24 = $carbon_time_string->addHours(24);
                $diff_in_time = $now->diffInMinutes($add_time_24);
                $possibility_write_for_user = $diff_in_time;
            } else {
                $possibility_write_for_user = 0;
            }
        }

        $elementVoteController = new ElementVoteRemoveController;

        $element_vote_remove_count = DB::table('element_vote_remove')
            ->where('element_id', '=', $element->id)
            ->count();

        if( isset(Auth::user()->id)) {
            $elementVote = $elementVoteController->get_vote($element->id, Auth::user()->id);
            $isCoAuthor = $this->isCoAuthor(Auth::user()->id, $element->id );
        }else{
            $elementVote = null;
            $isCoAuthor = null;
        }

        return view( $view, [
            'element' => $array['element_arr'],
            'element_strings' => $array['element_string'],
            'possibility_write_for_user' => $possibility_write_for_user,
            'users_group' => $users_group,
            'elementVote' => $elementVote,
            'isCoAuthor' => $isCoAuthor,
            'element_vote_remove_count' => $element_vote_remove_count
        ]);
    }


    /**
     *
     * @param Element $element
     * @param CorrectionsController $corrections
     * @return Factory|View
     */
    public function element_finished(Element $element,  Request $request)
    {
        // определяем шаблон. табличный или для чтения
        if (strpos($request->url(), 'read')) {
            $view = 'elements.element_finished_read';
        } else {
            $view = 'elements.element_finished';
        }

        // Закладки
        $BookmarksController = new BookmarksController;
        $bookmark = null;
        if (isset(Auth::user()->id)) {
            $bookmark = $BookmarksController->get_bookmark($element->id, Auth::user()->id);
        }

        // Рейтинг
        $RatingController = new RatingController;
        $ratings = $RatingController->get_by_entity($this->ratingable_type, $element->id);

        $array = $this->get_element($element->id);

        $corrections = new CorrectionsController;
        $count_corrections = $corrections->get_count_corrections_by_element($element->id);

        $ReviewsController = new ReviewsController;
        $count_reviews = $ReviewsController->get_count_reviews_by_element($element->id, 1);

        // Checking can the user to write
        if (isset(Auth::user()->id)) {
            $ability_write_review = $ReviewsController->get_ability_write_for_user(Auth::user()->id, $element->id);
            $ability_write_correction = $corrections->get_ability_write_for_user(Auth::user()->id, $element->id);
        } else {
            $ability_write_review = false;
            $ability_write_correction = false;
        }


        if (isset(Auth::user()->id)) {
            if (isset($request->page)) {
                $page_number = $request->page;
            } else {
                $page_number = 1;
            }
            $BookmarksController->set_bookmark($page_number, $element->id, Auth::user()->id);
        }

        $isAuthor = null;
        if (isset(Auth::user()->id)) {
            $isAuthor = $this->isCoAuthor(Auth::user()->id, $element->id);
        }

        return view($view, [
            'element' => $array['element_arr'],
            'element_strings' => $array['element_string'],
            'count_reviews' => $count_reviews,
            'count_corrections' => $count_corrections,
            'bookmark' => $bookmark,
            'ratings' => $ratings->all(),
            'ability_write_review' => $ability_write_review,
            'ability_write_correction' => $ability_write_correction,
            'is_author' => $isAuthor
        ]);
    }



    /**
     * @param $element_id
     * @return array
     */
    public function get_element($element_id)
    {
        $element_string = DB::table('element_strings AS es')
                ->select('es.id AS es_id', 'es.created_at AS created_at', 'es.string AS string', 'u.nic AS u_nic', 'u.name AS user_name', 'u.id AS user_id'
                )
                ->leftJoin('users AS u', 'u.id', '=', 'es.user_id')
                ->where('element_id', $element_id)
                ->paginate(20);

        $element_arr = DB::table('elements AS e')
                ->leftJoin('genres AS g', 'genre_id', '=', 'g.id')
                ->leftJoin('element_strings AS es', 'es.element_id', '=', 'e.id')
                ->leftJoin('users AS u', 'u.id', '=', 'es.user_id')
                ->leftJoin('ratings AS r', function($join) {
                    $userId = '';
                    if (isset(Auth::user()->id)) {
                        $userId = Auth::user()->id;
                    }
                    $join->on('e.id', '=', 'r.ratingable_id')
                    ->where('r.ratingable_type', '=', $this->ratingable_type)
                    ->where('r.user_id', '=', $userId);
                })
                ->where('e.id', '=', $element_id)
                ->select('e.id AS e_id', 'e.message', 'e.type',
                    'e.name AS e_name', 'e.created_at AS e_created_at', 'e.updated_at AS e_updated_at',
                    'e.parent_id AS e_parent_id', 'e.genre_id AS genre_id', 'blocked_user_id',
                    'e.blocked AS e_blocked', 'e.finished AS e_finished', 'e.slug AS e_slug', 'blocked_time'
                    ,'e.group AS e_group', 'e.published AS e_published'
                    ,'u.id AS user_id', 'u.name AS user_name', 'u.nic AS u_nic'
                    ,'g.name AS g_name', 'g.slug AS g_slug'
                    ,'r.score AS rating_score'

                )
                ->orderBy('es.id', 'ASC')
                ->first();

        $array['element_string'] = $element_string;
        $array['element_arr'] = $element_arr;
        return $array;
    }



    /**
     * @param $element_id
     * @return array
     */
    public function get_users_group_by_element($element_id)
    {
        return DB::table('element_group AS e_g')
                        ->leftJoin('users AS u', 'u.id', '=', 'e_g.user_id')
                        ->where('element_id', '=', $element_id)
                        ->get();
    }



    /**
     * Черновики
     * @param
     * @return View
     */
    public function elements_draft()
    {
        $elements = DB::table('elements AS e')
            ->leftJoin('genres AS g', 'genre_id', '=', 'g.id')
            ->select('e.id as e_id', 'e.message', 'e.type', 'e.name AS e_name',
                'e.created_at AS e_created_at', 'e.updated_at AS e_updated_at', 'e.parent_id AS e_parent_id',
                'e.genre_id AS genre_id', 'blocked_user_id', 'e.blocked AS e_blocked',
                'e.finished AS e_finished', 'e.slug AS e_slug', 'e.blocked_time',
                'g.name AS g_name', 'g.slug AS g_slug', 'g.id AS genre_id'
            )
            ->where('e.published', '=', 0)
            ->groupBy('e.id')
            ->orderBy('e.id', 'DESC')
            ->paginate(20);

        return view('elements.elements_draft',
            ['elements' => $elements]
        );
    }


    /**


     */
    public function store(Request $request)
    {
        $errors = false;
        $badWordController = new BadWordController();
        $esBadWord = $badWordController->isBadWord($request->input('elements_string'));
        $nameBadWord = $badWordController->isBadWord($request->input('name'));
        $messageBadWord = $badWordController->isBadWord($request->input('message'));

        if($esBadWord || $nameBadWord || $messageBadWord){
            $errors = 'bad-word';
            $message = 'У вас используется неприемлимое слово';
        }

        if ($request->get('group') && !$request->get('authors')) {
            //return redirect()->route('element.create');
            $errors = 'not-author';
            $message = 'При групповом произведении нужно выбрать автора(ов)';
        }

        if($errors){
            return json_encode([
                'success' => false,
                'errors' => $errors,
                'message' => $message,
            ]);
        }

        $element_res = Element::create($request->all());
        $element = Element::find($element_res->id);
        $element_string = new ElementString(['string' => $request->input('elements_string'), 'user_id' => Auth::user()->id]);

        $element->elements_string()->save($element_string);

        // сохраняем авторов, есть они есть и произведение закрытое
        if ($request->get('group') && $request->get('authors')) {
            $result = DB::table('element_group')->insert(
                    ['element_id' => $element_res->id, 'user_id' => Auth::user()->id]
            );

            $user_from = ['id' => Auth::user()->id, 'nic' => Auth::user()->nic];
            foreach ($request->get('authors') as $key => $val) {
                DB::table('element_group')->insert(
                        ['element_id' => $element_res->id, 'user_id' => $val]
                );
            }

            // получаем коллекцию авторов
            $users = User::whereIn('id', $request->get('authors') )->get();
            // отправляем уведомления авторам
            Notification::send($users, new \App\Notifications\ElementGroup($element, $user_from));
        }

        $url_redirect = route( 'element.show',  $element_res->id);
        return json_encode([
            'success' => true,
            'url_redirect' => $url_redirect,
        ]);
    }


    /**
     * Проверяем тест на нецензурную речь
     *
     * @param String $text
     */
    public function validationText($text)
    {
        $arr = ['блять', 'хуй'];
        foreach ($arr as $str){
            $res = strpos($text, $str);
            if($res !== false){
                return false;
            }
        }
        return true;
    }


    public function edit(Element $element)
    {
        return view('elements.edit_element', [
            'element' => $element
        ]);
    }

    public function update(Request $request, Element $element)
    {
        $element->update($request->except('slug'));
        return redirect()->route('element');
    }

    public function destroy(Element $element)
    {
        $element->delete();
        return redirect()->route('element');
    }



    /**
     * Произведения, которые читает пользователь
     * Works of art, which reading by user
     *
     * @return View
     */
    public function reader()
    {
        $elements = DB::table('elements AS e')
                ->leftJoin('genres AS g', 'e.genre_id', '=', 'g.id')
                ->leftJoin('bookmarks AS b', 'b.element_id', '=', 'e.id')
                ->select('e.id AS e_id', 'e.message', 'e.type', 'e.name AS e_name',
                    'e.created_at AS e_created_at', 'e.updated_at AS e_updated_at', 'e.parent_id AS e_parent_id',
                    'e.genre_id as genre_id', 'blocked_user_id', 'e.blocked AS e_blocked',
                    'e.finished AS e_finished', 'e.slug AS e_slug', 'e.blocked_time',
                    'g.name AS g_name', 'g.slug AS g_slug', 'g.id AS genre_id'
                )
                ->where(function($query) {
                    $query->where('e.created_at', '<', Carbon::now()->subDays(14))
                    ->orWhere('e.finished', '=', 1);
                })
                ->where('b.user_id', '=', Auth::user()->id)
                ->where('e.published', '=', 1)
                ->groupBy('e.id')
                ->orderBy('e.id', 'DESC')
                ->paginate(20);
        return view('elements.reader',
                ['elements' => $elements]
        );
    }


    /**
     * Произведения, которые пишет авторизованный автор
     * Works written by an authorized author
     *
     * @return View
     */
    public function writing()
    {
        $elements = DB::table('element_strings AS e_s')
            ->select('e_s.element_id AS e_id', 'e.name AS e_name', 'e.message',
                'e.type', 'e.created_at AS e_created_at', 'e.group AS e_group',
                'g.id AS genre_id', 'g.name AS g_name',

                DB::raw("(SELECT COUNT(es.id) FROM element_strings AS es WHERE es.element_id = e_s.element_id ) AS count_element_strings")
            )
            ->leftJoin('elements AS e', 'e.id', '=', 'e_s.element_id')
            ->leftJoin('genres AS g', 'e.genre_id', '=', 'g.id')
            ->where('e_s.user_id', '=', Auth::user()->id)
            ->where('e.created_at', '>', Carbon::now()->subDays(14))
            ->where('e.published', '=', 1)
            ->groupBy('e_s.element_id')
            ->orderBy('e_s.element_id', 'DESC')
            ->paginate();

        return view('elements.writing',
            ['elements' => $elements]
        );
    }


    /**
     * Произведения, которые написал авторизованный автор
     * Works, which were written by an authorized author
     *
     * @return View
     */
    public function wrote()
    {
        $elements = DB::table('element_strings AS es')
            ->Join('users AS u', 'u.id', '=', 'es.user_id')
            ->leftJoin('elements AS e', 'e.id', '=', 'es.element_id')
            ->leftJoin('genres AS g', 'e.genre_id', '=', 'g.id')
            ->select(
                'e.id AS e_id', 'e.message', 'e.type', 'e.name AS e_name', 'e.created_at AS e_created_at'
                ,'e.genre_id AS genre_id'
                ,'g.name AS g_name', 'g.slug AS g_slug', 'g.id AS genre_id'

//                ,'u.nic AS u_nic', 'u.name AS u_name', 'u.id AS u_id'
            )
            ->where('u.id', '=', Auth::user()->id)
            ->where('e.created_at', '<', Carbon::now()->subDays(14))
            ->where('e.published', '=', 1)
            ->groupBy('es.element_id')
            ->orderBy('es.element_id', 'DESC')
//            ->toSql();
            ->paginate(20);

        //echo '<xmp>';
        //print_r( $elements->all() );
        //echo '</xmp>';

//        dd( $elements );

        return view('elements.wrote',
                ['elements' => $elements]
        );
    }


    /**
     * Checking if the Element has been finished or works
     * Проверяем опубликован ли Element. Возвращаем Null  для НЕ оконченного или коллекцию для оконченного .
     * @param $element_id
     * @return mixed. Null for works. Collecion for finished.
     */
    public function isFinishedElement($element_id )
    {
        return Element::where('id', '=', $element_id)
            ->where(function($query) {
                $query->where('created_at', '<', Carbon::now()->subDays(14))
                    ->orWhere('finished', '=', 1);
            })
            ->first();
    }


    /**
     * Checking for a co-author
     * Проверяем на соавтора
     * @param $user_id
     * @param $element_id
     * @return mixed
     */
    public function isCoAuthor($user_id, $element_id)
    {
        return ElementString::where('user_id', '=', $user_id)->where('element_id', '=', $element_id)->first();
    }


    public function isPublished($element_id)
    {
        return Element::where('id', '=', $element_id)
            ->where('published', '=', 1)
            ->first();
    }

}
