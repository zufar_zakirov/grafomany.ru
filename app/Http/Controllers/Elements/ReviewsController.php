<?php

namespace App\Http\Controllers\Elements;

use App\ElementString;
use App\Http\Controllers\BadWordController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\User\UserController;
use App\User;
use App\Http\Controllers\Elements;
use App\Element;
use App\Review;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ReviewsController extends Controller
{

    public $perPage = '20';
    public $ratingable_type = 'review';


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($element_id)
    {
        $element = Element::find( $element_id);
        $element_string = DB::table('element_strings')
            ->leftJoin('users', 'users.id', '=', 'element_strings.user_id')
            ->where('element_id', $element_id)
            ->get();
        return view('reviews.create', [
            'element' => $element,
            'element_strings' => $element_string
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Element $element
     * @return Response
     */
    public function store(Request $request, Element $element)
    {
        $badWordController = new BadWordController();
        $textBadWord = $badWordController->isBadWord($request->input('text'));
        if($textBadWord){
            return json_encode([
                'success' => false,
                'errors' => 'bad-word',
                'message' => 'У вас используется неприемлимое слово',
            ]);
        }

        $review_res = Review::create(
            [
                'text' => $request->input('text'),
                'user_id' => Auth::user()->id,
                'element_id' => $element->id,
                'published' => $request->input('published'),
            ]
        );
        $review = Review::find($review_res->id);

        if($request->input('published')){
            $url_redirect = route('review.show', $review->id);
        }else{
            $url_redirect = route('review.edit', $review->id);
        }

        return json_encode([
            'success' => true,
            'url_redirect' => $url_redirect,
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param Review $review
     * @return Response
     */
    public function show( Review $review )
    {
        $result = DB::table('reviews')
            ->select('reviews.id AS review_id',
                'reviews.text AS review_text',
                'reviews.created_at AS review_created_at' ,
                'reviews.published AS review_published',
                'elements.name AS element_name' ,
                'elements.id AS element_id' ,
                'elements.created_at AS element_created_at',
                'genres.name AS genre_name',
                'genres.id AS genre_id',
                'users.id AS user_id',
                'users.name AS user_name',
                'users.nic AS user_nic',
                'r.score AS rating_score'
            )
            ->leftJoin('elements', 'elements.id', '=', 'reviews.element_id')
            ->leftJoin('genres', 'elements.genre_id', '=', 'genres.id')
            ->leftJoin('users', 'users.id', '=', 'reviews.user_id')
            ->leftJoin('ratings AS r', function($join){
                $userId = '';
                if(isset(Auth::user()->id)){
                    $userId = Auth::user()->id;
                }
                $join->on('reviews.id', '=', 'r.ratingable_id')
                    ->where('r.ratingable_type', '=', $this->ratingable_type)
                    ->where('r.user_id', '=', $userId );
            })
            ->where('reviews.id', '=', $review->id)
            ->first();

        // Рейтинг
        $RatingController = new RatingController;
        $ratings = $RatingController->get_by_entity($this->ratingable_type, $review->id);

        return view('reviews.show', [
                'review' => $result,
                'Mreview' => $review,
                'ratings' => $ratings
            ]
        );
    }



    /**
     * Display Reviews for the User
     *
     * @param $user_id
     * @return Response
     */
    public function show_reviews_by_user($user_id)
    {
        $items = $this->get_reviews_by_user( $user_id );
        $userController = new UserController;
        $user = $userController->get_user($user_id);

        return view('reviews.show_by_user', [
                'items' => $items,
                'user' => $user
            ]
        );
    }

    /**
     * Return Reviews for the User
     *
     * @param $user_id
     * @param int $published
     * @return \Illuminate\Support\Collection
     */
    public function get_reviews_by_user($user_id , $published = 1, $perPage = 30)
    {
        $items = DB::table('reviews AS r')
            ->leftJoin('users AS u', 'u.id', '=', 'r.user_id')
            ->leftJoin('elements AS e', 'e.id', '=', 'r.element_id')
            ->leftJoin('ratings AS rtg', function ($join){
                $join->on( 'ratingable_id', '=', 'r.id')
                    ->where('ratingable_type' , '=', 'review');
            })
            ->where('u.id' ,'=', $user_id)
            ->where('r.published', '=', $published)
            ->select('e.name AS element_name',
                'e.id AS element_id',
                'e.created_at AS element_created_at',
                'r.id AS review_id',
                'r.text AS review_text',
                'r.created_at AS review_created_at',
                'u.id AS user_id',
                'u.nic AS user_nic'
            )
            ->selectRaw('SUM(score=3) AS score_3, SUM(score=2) AS score_2, SUM(score=1) AS score_1')
            ->orderBy('r.created_at', 'DESC')
            ->groupBy('r.id')
            ->paginate($perPage);

        $complite_date = $this->ratingToObject($items);

        return $complite_date;
    }

    /**
     * Return Count Reviews for User
     *
     * @param $user_id
     * @param int $published
     * @return int
     */
    public function get_count_reviews_by_user($user_id, $published = 1)
    {
        $count = DB::table('reviews')
            ->where('reviews.user_id', '=', $user_id)
            ->where('reviews.published', '=', $published)
            ->count('reviews.id');
        return $count;
    }

    /**
     * Return Count Reviews for Element
     *
     * @param $element_id
     * @param int $published
     * @return int
     */
    public function get_count_reviews_by_element($element_id, $published = 1)
    {
        $count = DB::table('reviews')
            ->where('reviews.element_id', '=', $element_id)
            ->where('reviews.published', '=', $published)
            ->count('reviews.id');

        return $count;
    }

    /**
     * Return Reviews for the Element
     *
     * @param $element_id
     * @return Response
     */
    public function show_reviews_by_element($element_id)
    {
        $items = $this->get_reviews_by_element($element_id, $this->perPage);
        return view('reviews.reviews_by_element', [
                'items' => $items
            ]
        );
    }

    /**
     * Return Reviews for the Element
     *
     * @param $element_id
     * @return $items
     */
    public function get_reviews_by_element($element_id, $perPage = 3)
    {
        $items = DB::table('reviews AS r')
            ->leftJoin('elements AS e', function ($join){
                $join->on('e.id', '=', 'r.element_id')
                    ->where('r.published', '=', 1);
            })
            ->leftJoin('ratings AS rtg', function ($join){
                $join->on( 'ratingable_id', '=', 'r.id')
                    ->where('ratingable_type' , '=', 'review');
            })
            ->leftJoin('users AS u', 'u.id', '=', 'r.user_id')
            ->where('e.id', $element_id)
            ->select('e.name AS element_name',
                'e.id AS element_id',
                'e.created_at AS element_created_at',
                'r.id AS review_id',
                'r.text AS review_text',
                'r.created_at AS review_created_at',
                'u.id AS user_id',
                'u.nic AS user_nic'
            )
            ->selectRaw('SUM(score=3) AS score_3, SUM(score=2) AS score_2, SUM(score=1) AS score_1')
            ->orderBy('r.created_at', 'DESC')
            ->groupBy('r.id')
            ->paginate($perPage);

        $complite_date = $this->ratingToObject($items);

        return $complite_date;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Review $review
     * @return void
     */
    public function edit(Review $review)
    {
        $item = DB::table('reviews AS r')
            ->select('r.id AS review_id', 'r.text AS review_text' ,
                'e.id AS element_id' , 'e.name AS element_name', 'e.published AS element_published')
            ->leftJoin('elements AS e', 'e.id', '=', 'r.element_id')
            ->where('r.id', '=', $review->id)
            ->first();

        return view('reviews.edit',[
            'item' => $item
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return void
     */
    public function reviews_in_work()
    {
        $reviews = $this->get_reviews_by_user_unpublished( Auth::user()->id );
        return view('reviews.reviews_by_user_unpublished', [
            'items' => $reviews
        ]);
    }

    /**
     * Return Reviews un published for the user
     *
     * @param $user_id
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function get_reviews_by_user_unpublished($user_id)
    {
        $result = DB::table('users AS u')
            ->select(
                'u.nic AS user_nic',
                'r.id AS review_id',
                'r.created_at AS review_created_at',
                'r.text AS review_text',
                'e.id AS element_id',
                'e.name AS element_name'
            )
            ->leftJoin('reviews AS r', 'r.user_id', '=', 'u.id')
            ->leftJoin('elements AS e', 'e.id', '=', 'r.element_id')
            ->where('u.id', '=', $user_id)
            ->where('r.published', '=', 0)
            ->paginate(20);
        return $result;
    }

    /**
     * @param $user_id
     * @param $element_id
     *
     * @return bool
     */
    public function get_ability_write_for_user($user_id, $element_id)
    {
        $res = Review::where('user_id', '=', $user_id)
            ->where('element_id', '=', $element_id)
            ->first();
        if($res){$result = false;} else{$result = true;}
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Review $review
     * @return void
     */
    public function update(Request $request, Review $review)
    {
        $badWordController = new BadWordController();
        $textBadWord = $badWordController->isBadWord($request->input('text'));
        if($textBadWord){
            return json_encode([
                'success' => false,
                'errors' => 'bad-word',
                'message' => 'У вас используется неприемлимое слово',
            ]);
        }

        $result = $review->update($request->all());

        if($request->input('published')){
            $url_redirect = route('review.show', $review->id);
        }else{
            $url_redirect = route('review.edit', $review->id);
        }

        return json_encode([
            'success' => true,
            'url_redirect' => $url_redirect,
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
