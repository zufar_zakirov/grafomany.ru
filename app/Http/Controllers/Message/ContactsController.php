<?php

namespace App\Http\Controllers\Message;

use App\Events\NewMessage;
use App\Http\Controllers\Controller;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ContactsController extends Controller
{
    public function get()
    {
        $contacts = User::where('id', '<>', auth()->id() )->get();
        return response()->json($contacts);
    }


    public  function getMes($chat_id)
    {
        //dd($request);
        $messages = DB::table('messages')
            ->leftJoin('users AS u', 'u.id', '=', 'messages.chat_id')
            ->select('u.nic AS user_nic', 'messages.chat_id AS chat_id', 'messages.text AS message_text', 'messages.created_at AS message_create')
            ->orderBy('messages.created_at', 'DESC')
            ->where('chat_id', '=', $chat_id)
            ->toSql();

        dd( $messages);
    }





    public function search($query)
    {

    }

}
