<?php

namespace App\Http\Controllers;

use Auth;
use App\Bookmark;
use Illuminate\Http\Request;

class BookmarksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }



    public function set_bookmark($page_number, $element_id, $user_id)
    {
        $res = Bookmark::create([
            'page_number' => $page_number,
            'element_id' => $element_id,
            'user_id' => $user_id
        ]);
        return $res;
    }


    public function remove_bookmark(Request $request)
    {
        $element_id = $request->input('element_id');

        if( isset( Auth::user()->id )){
            Bookmark::where('user_id', '=', Auth::user()->id )
                ->where('element_id', '=', $element_id)
                ->delete();
        }
        return redirect()->route('reader');
    }


    public function get_bookmark($element_id, $user_id)
    {
        $item = Bookmark::where('element_id',$element_id)
            ->where('user_id', $user_id)
            ->orderBy('id', 'DESC')
            ->first();
        if($item)
            $res = $item->page_number;
        else
            $res = NULL;
        return $res;
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
