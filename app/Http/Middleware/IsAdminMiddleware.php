<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class IsAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( isset(Auth::user()->id) && 3 === (int)Auth::user()->role_id){
            return $next($request);
        }
        return redirect()->route('elements');

    }
}

