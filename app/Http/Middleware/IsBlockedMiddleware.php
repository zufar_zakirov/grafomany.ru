<?php

namespace App\Http\Middleware;

use Auth;
use App\Element;
use Closure;
use Carbon\Carbon;

class IsBlockedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next )
    {
        $arr = explode('/', $request->path() );

        $element_id = $arr[1];

        $element = Element::where('id', '=', $element_id )
            //->where('blocked_time', '>', Carbon::now() )
            ->first();


        if($element->blocked_time > Carbon::now()  && $element->blocked_user_id != Auth::user()->id ){
            return redirect()->route('element.show', $element_id);
        }else{
            return $next($request);
        }

    }
}
