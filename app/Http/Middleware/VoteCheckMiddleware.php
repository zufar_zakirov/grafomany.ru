<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use App\ElementVoteRemove;

class VoteCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( $request->element_id ){
            $element_id = $request->element_id;
            $res = ElementVoteRemove::where('element_id', '=', $element_id )
                ->where('user_id', '=', Auth::user()->id)
                ->first();
            if( $res ){
                return redirect()->route('element.show', $element_id);
            }
        }

        return $next($request);
    }
}
