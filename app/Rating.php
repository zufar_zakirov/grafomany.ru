<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = [
        'user_id', 'ratingable_id', 'ratingable_type', 'score'
    ];


    public function ratingable()
    {
        return $this->morphTo();
    }

}
