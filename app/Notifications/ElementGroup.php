<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

// implements ShouldQueue
class ElementGroup extends Notification
{
//    use Queueable;

    private $info;
    private $user_from;
    private $title = 'Вас пригласили в групповое произведение!';

    /**
     * Create a new notification instance.
     *
     * @param $info
     */
    public function __construct($info, $user_from)
    {
        $this->info = $info;
        $this->user_from = $user_from;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //'mail',
        return [ 'database'];
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'invite',
            'title' => $this->title,
            'element' => $this->info,
            'user_from' => $this->user_from,
        ];
    }
}
