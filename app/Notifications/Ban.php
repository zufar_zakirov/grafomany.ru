<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

// implements ShouldQueue
class Ban extends Notification
{
//    use Queueable;

    private $title = 'Вас заблокировали';
    private $ban_time;
    private $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ban_time, $message, $ban)
    {
        $this->ban_time = $ban_time;
        $this->message = $message;
        $this->ban = $ban;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //'mail',
        return [ 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'ban',
            'title' => $this->title,
            'message' => $this->message,
            'ban_time' => $this->ban_time,
            'ban' => $this->ban
        ];
    }
}
