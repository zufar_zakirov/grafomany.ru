<?php
namespace App\Helpers\Date;

class DateFormat{
    public static $format1 = 'H:i d.m.Y';
    public static $format2 = 'H:i / d.m.Y';
    public static $format3 = 'H:i  d/m/Y';

    /**
     * @param $date
     *
     * @return string
     */
    public static function get_date_format1($date)
    {
        return date( self::$format1, strtotime($date));
    }

}
