<?php

namespace App\Policies;

use App\Correction;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CorrectionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any corrections.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the correction.
     *
     * @param  \App\User  $user
     * @param  \App\Correction  $correction
     * @return mixed
     */
    public function view(User $user, Correction $correction)
    {
        //
    }


    /**
     * Determine whether the user can create corrections.
     *
     * @param  User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }


    /**
     * Determine whether the user can update the correction.
     *
     * @param  User  $user
     * @param  Correction  $correction
     * @return mixed
     */
    public function update(User $user, Correction $correction)
    {
        return $user->id === $correction->user_id;
    }



    /**
     * Determine whether the user can form edit the correction.
     *
     * @param  User $user
     * @param  Correction $correction
     * @return  mixed
     */
    public function edit(User $user, Correction $correction)
    {
        if( 0 === (int)$correction->published
            && (int)$user->id === (int)$correction->user_id ){
            return true;
        }
        return false;
    }



    /**
     * Determine whether the user can delete the correction.
     *
     * @param  \App\User  $user
     * @param  \App\Correction  $correction
     * @return mixed
     */
    public function delete(User $user, Correction $correction)
    {
        //
    }



    /**
     * Determine whether the user can restore the correction.
     *
     * @param  \App\User  $user
     * @param  \App\Correction  $correction
     * @return mixed
     */
    public function restore(User $user, Correction $correction)
    {
        //
    }



    /**
     * Determine whether the user can permanently delete the correction.
     *
     * @param  \App\User  $user
     * @param  \App\Correction  $correction
     * @return mixed
     */
    public function forceDelete(User $user, Correction $correction)
    {
        //
    }
}
