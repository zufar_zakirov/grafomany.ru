<?php

namespace App\Policies;


use App\Review;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReviewPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the review.
     *
     * @param  User $user
     * @param  Review $review
     * @return mixed
     */
    public function update(User $user, Review $review)
    {
        return $user->id === $review->user_id;
    }


    /**
     * Determine whether the user can to edit the review.
     *
     * @param User $user
     * @param Review $review
     * @return mixed
     */
    public function edit(User $user, Review $review)
    {
        if( 0 === (int)$review->published
            && (int)$user->id === (int)$review->user_id ){
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Determine
     *
     * @param User $user
     * @param Review $review
     */
    public function review_published(User $user, Review $review)
    {
        //echo 'sdf';
        return false;
//        if( $review->published === 0 && $user->id === $review->user_id ){
//            return TRUE;
//        }
//        return FALSE;
    }


    public function reviews_in_work(User $user)
    {
        return true;
        //return $user->id === Auth::user()->id;
    }

}
