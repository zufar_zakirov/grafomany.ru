<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElementString extends Model
{
    protected $fillable = [ 'id' , 'element_id' , 'user_id', 'string', 'published', 'created_at', 'updated_at' ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
