<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Element extends Model
{
    protected $fillable = [ 'id' , 'name' , 'slug', 'message', 'genre_id', 'text', 'group', 'published', 'created_at', 'updated_at' ];


    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug( mb_substr( $this->name, 0 , 40 ) ."-". \Carbon\Carbon::now()->format('dmyHi'), '-' ) ;
    }


    public function elements_string()
    {
        return $this->hasMany('App\ElementString');
    }

    public function genre()
    {
        return $this->belongsTo('App\Genre');
    }

    public function ratings()
    {
        return $this->morphMany('App\Rating', 'ratingable');
    }



}
