<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BadWord extends Model
{
    protected $table = 'bad_words';
    protected $fillable = [ 'id' , 'word', 'updated_at', 'created_at' ];
    protected $guarded = [];
}
