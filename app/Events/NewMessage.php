<?php

namespace App\Events;

use App\Message;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use App\User;

class NewMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        Log::info( $message );
        //$user = User::where('id', '=', $message->user_id )->first();
        $m = [];
        $m['user_id']    = $message['user_id'];
        $m['text']       = $message['text'];
        $m['chat_id']    = $message['chat_id'];
        $m['created_at'] = $message['created_at'];
        $m['id']         = $message['id'];
        $m['user_nic']   = $message['user_nic'];

        $this->message = $m;
        Log::info( $m );
        $this->dontBroadcastToCurrentUser();
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        //return new PrivateChannel('messages'  );
        return new Channel('messages'. $this->message['chat_id']);
    }


//    public function broadcastWith()
//    {
//        return["message" => $this->message];
//    }

}
