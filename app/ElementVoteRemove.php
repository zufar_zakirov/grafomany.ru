<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElementVoteRemove extends Model
{
    //protected $connection = '';
    protected $table = 'element_vote_remove';
}
