<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MessagesController;
use App\Http\Controllers\ChatsController;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->isNewMessage();
        //
        Schema::defaultStringLength(191);
    }

    public function isNewMessage2()
    {
        $chatController = new ChatsController;
        View::composer('partials.count_new_message', function($view) use($chatController){
           if(Auth::check()){
               $count_new_message = $chatController->getCountUnreadMessagesByUser();
               $view->with('count_new_message', 55 );
           }
        });
    }

    public function isNewMessage()
    {
        View::composer('partials.count_new_message', function($view){
            if(Auth::check()){
                $chatController = new ChatsController;
                $count_new_message = $chatController->getCountUnreadMessagesByUser();
                $view->with('count_new_message', $count_new_message );
            }
        });
    }

}
