<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

//use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\NotificationsController;
use Illuminate\Support\Facades\View;

class NoticeCountServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(NotificationsController $notification)
    {
        View::composer('partials.notice_count', function($view) use ($notification) {
            if(Auth::check()){
                $view->with('noticeCount', $notification->getNoticeCount() );
            }

            //$view->with(['noticeCount' => '4561234dfs']);
            //$currentUser = $auth->user();
            //$view->with(['noticeCount' => NotificationsController::getNoticeCount()]);
        });
    }

    public function getNoticeCount()
    {
    }
}
