<?php

namespace App\Providers;
use App\Policies\AdminPolicy;
use App\User;
use App\Policies\UserPolicy;
use App\Correction;
use App\Policies\CorrectionPolicy;
use App\Review;
use App\Policies\ReviewPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Correction::class => CorrectionPolicy::class,
        Review::class => ReviewPolicy::class,
        User::class => UserPolicy::class,
        Admin::class => AdminPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
