<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Correction extends Model
{
    protected $fillable = [ 'id' , 'element_id' , 'user_id', 'text', 'published', 'created_at', 'updated_at' ];
}
