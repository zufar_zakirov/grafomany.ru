<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageStatus extends Model
{
    protected $fillable = ['id', 'user_id', 'is_read', 'created_at', 'updated_at', 'message_id'];
    protected $table = 'message_status';
    protected $guarded = [];

    private $message_id;
}
