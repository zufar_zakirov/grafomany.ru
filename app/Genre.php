<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Genre extends Model
{
    protected $fillable = [ 'id' , 'slug', 'name' , 'text', 'letters_for_string', 'published', 'created_at', 'updated_at', 'number_lines' ];
    //'title_meta', 'desc_meta', 'key_meta',

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = Str::slug( mb_substr( $this->name, 0 , 40 ) ."-". \Carbon\Carbon::now()->format('dmyHi'), '-' ) ;
    }

    public function elements()
    {
        return $this->hasMany('App\Element');
    }

}
